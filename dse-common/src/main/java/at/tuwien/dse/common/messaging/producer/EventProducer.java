package at.tuwien.dse.common.messaging.producer;

import io.swagger.model.AVUpdateEvent;
import io.swagger.model.CrashEventDTO;


/**
 * This class is used to produce different types of events for the messaging queue used.
 * */
public interface EventProducer {

    /**
     * Takes an AvUpdateEvent and pushes it to the matching messaging queue.
     * @param updateEvent
     * */
    void sendUpdateEvent(AVUpdateEvent updateEvent);

    /**
     * Takes an AvUpdateEvent and pushes it to the matching messaging queue.
     * @param crashEvent
     * */
    void sendCrashEvent(CrashEventDTO crashEvent);
}

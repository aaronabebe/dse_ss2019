package at.tuwien.dse.common.messaging.producer;

import io.swagger.model.AVUpdateEvent;
import io.swagger.model.CrashEventDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import static at.tuwien.dse.common.messaging.configuration.KafkaConstants.CRASH_EVENT_TOPIC;
import static at.tuwien.dse.common.messaging.configuration.KafkaConstants.UPDATE_EVENT_TOPIC;

@Service
public class KafkaEventProducer implements EventProducer {

    private static final Logger logger = LoggerFactory.getLogger(KafkaEventProducer.class);

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    @Override
    public void sendUpdateEvent(AVUpdateEvent updateEvent) {
        logger.info(String.format("$$ -> Producing Event -> %s", updateEvent));
        this.kafkaTemplate.send(UPDATE_EVENT_TOPIC, "update", updateEvent);
    }

    @Override
    public void sendCrashEvent(CrashEventDTO crashEvent) {
        logger.info(String.format("$$ -> Producing Notification -> %s", crashEvent));
        this.kafkaTemplate.send(CRASH_EVENT_TOPIC, "crash", crashEvent);
    }
}

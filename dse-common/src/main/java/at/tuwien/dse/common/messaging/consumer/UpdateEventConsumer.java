package at.tuwien.dse.common.messaging.consumer;

import at.tuwien.dse.common.messaging.configuration.KafkaConstants;
import io.swagger.model.AVUpdateEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;


/**
 * Implement this class to enable consuming CrashEvents from a Kafka queue.
 * */
public interface UpdateEventConsumer {

    /**
     * Consumes incoming AvUpdateEvents.
     *
     * @param cr incoming consumer record
     */
    @KafkaListener(topics = KafkaConstants.UPDATE_EVENT_TOPIC, clientIdPrefix = "json", groupId = KafkaConstants.GROUP_ID)
    void consumeUpdateEvent(ConsumerRecord<String, AVUpdateEvent> cr);
}

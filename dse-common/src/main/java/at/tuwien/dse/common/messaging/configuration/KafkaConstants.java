package at.tuwien.dse.common.messaging.configuration;

public final class KafkaConstants {

    private KafkaConstants(){}

    public static final String UPDATE_EVENT_TOPIC = "update-event-topic";

    public static final String CRASH_EVENT_TOPIC = "crash-event-topic";

    public static final String GROUP_ID = "dse-queue";

    public static final String AUTO_OFFSET_RESET_CONFIG = "earliest";

}

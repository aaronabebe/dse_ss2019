package at.tuwien.dse.common.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Document(collection = "oems")
public class OEM implements Serializable {

    @Id
    private String name;

    private List<AV> avs = new ArrayList<>();

    public OEM() {
    }

    public OEM(String name) {
        this.name = name;
    }

    public OEM(String name, List<AV> avs) {
        this.name = name;
        this.avs = avs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean addAV(AV av) {
        if (this.avs.stream().noneMatch(p -> p.getChassisNumber().equals(av.getChassisNumber()))) {
            this.avs.add(av);
            return true;
        }
        return false;
    }

    public void removeAV(String chassisNumber) {
        this.avs = this.avs.stream()
            .filter(av -> !av.getChassisNumber().equals(chassisNumber))
            .collect(Collectors.toList());
    }

    public AV getAV(String chassisNumber) {
        return this.avs.stream()
            .filter(av -> av.getChassisNumber().equals(chassisNumber))
            .findAny()
            .orElse(null);
    }

    public List<AV> getAVs() {
        return avs;
    }

    public void setAVs(List<AV> avs) {
        this.avs = avs;
    }

    public String toString() {
        return String.format("[%s: %s]", name, avs);
    }
}

package at.tuwien.dse.common.messaging.consumer;

import at.tuwien.dse.common.messaging.configuration.KafkaConstants;
import io.swagger.model.CrashEventDTO;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;

/**
 * Implement this class to enable consuming CrashEvents from a Kafka queue.
 * */
public interface CrashEventConsumer {

    /**
     * Consumes incoming CrashEvents and then builds notifications for the different actors in the system.
     *
     * @param cr incoming consumer record
     */
    @KafkaListener(topics = KafkaConstants.CRASH_EVENT_TOPIC, clientIdPrefix = "json", groupId = KafkaConstants.GROUP_ID)
    void consumeCrashEvent(ConsumerRecord<String, CrashEventDTO> cr);
}

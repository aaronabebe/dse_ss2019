package at.tuwien.dse.common.domain;

import org.springframework.data.annotation.Id;


public class AvPosition {

    @Id
    private String chassisNumber;

    private Double latitude;

    private Double longitude;

    private String oem;

    public AvPosition() {
    }

    public String toString() {
        return String.format("AvPosition[%s, %s, %f, %f]", chassisNumber, oem, latitude, longitude);
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getOem() {
        return oem;
    }

    public void setOem(String oem) {
        this.oem = oem;
    }
}

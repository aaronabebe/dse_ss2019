package at.tuwien.dse.common.domain;

import io.swagger.model.AVUpdateEvent;
import io.swagger.model.AccidentStatus;
import io.swagger.model.CrashEventDTO;
import io.swagger.model.Event;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "accidents")
public class Accident implements Serializable {

    @Id
    private String id;
    private String description;
    private AccidentStatus status;
    private CrashEventDTO event;

    public Accident() {
    }

    public Accident(String description, AccidentStatus status, CrashEventDTO event) {
        this.description = description;
        this.status = status;
        this.event = event;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AccidentStatus getStatus() {
        return status;
    }

    public void setStatus(AccidentStatus status) {
        this.status = status;
    }

    public String toString() {
        return String.format("[%s, %s, %s]", id, description, status);
    }

    public CrashEventDTO getEvent() {
        return event;
    }

    public void setEvent(CrashEventDTO event) {
        this.event = event;
    }

//    public static PropertyMap<Accident, AccidentDTO> getPropertyMap() {
//        return new PropertyMap<Accident, AccidentDTO>() {
//            @Override
//            protected void configure() {
//                map().id(source.id);
//                map().description(source.description);
//                map().status(source.status);
//                map().event(source.event);
//            }
//        };
//    }
}

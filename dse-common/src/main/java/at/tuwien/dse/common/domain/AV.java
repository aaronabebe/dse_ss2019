package at.tuwien.dse.common.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;

import java.io.Serializable;

public class AV implements Serializable {

    @Id
    private String chassisNumber;

    private String modelType;

    @Transient
    private OEM oem;

    public AV() {
    }

    public AV(String modelType, String chassisNumber, OEM oem) {
        this.modelType = modelType;
        this.chassisNumber = chassisNumber;
        this.oem = oem;
    }

    public String getModelType() {
        return modelType;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String toString() {
        return String.format("[%s, %d]", modelType, chassisNumber);
    }


}
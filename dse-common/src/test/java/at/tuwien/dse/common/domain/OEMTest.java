package at.tuwien.dse.common.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class OEMTest {

    @Test
    public void testAddAV() {
        OEM oem = new OEM();
        AV av = new AV("Lexus", "1234", null);
        oem.addAV(av);
        oem.addAV(new AV("X5", "1234", null));
        assertEquals(1, oem.getAVs().size());
        assertEquals(av, oem.getAV("1234"));
    }

    @Test
    public void testRemoveAV() {
        OEM oem = new OEM();
        AV av = new AV("Lexus", "1234", null);
        oem.addAV(av);
        oem.removeAV(av.getChassisNumber());
        assertEquals(0, oem.getAVs().size());
    }
}

package at.tuwien.dse.common.eventstore;

import at.tuwien.dse.eventstore.repository.EventHistoryRepository;
import at.tuwien.dse.eventstore.repository.PositionRepository;
import at.tuwien.dse.eventstore.service.EntityStoreService;
import at.tuwien.dse.eventstore.service.EventStoreService;
import at.tuwien.dse.eventstore.service.EventStoreServiceImpl;
import io.swagger.model.AVUpdateEvent;
import io.swagger.model.CrashEventDTO;
import io.swagger.model.Event;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class EventStoreServiceTest {

    @MockBean
    private ModelMapper modelMapper;
    @MockBean
    private EventHistoryRepository eventHistoryRepository;
    @MockBean
    private EntityStoreService entityStoreService;
    @MockBean
    private PositionRepository positionRepository;
    @MockBean
    private SimpMessagingTemplate simpMessagingTemplate;

    private EventStoreService eventStoreService;

    @Before
    public void setup() {
        eventStoreService = new EventStoreServiceImpl(modelMapper, null, eventHistoryRepository,
            entityStoreService, positionRepository, simpMessagingTemplate);
    }

    @Test
    public void givenValidEventId_whenGetEvent_expectEvent() {
        var event = new AVUpdateEvent();
        event.setCrashEventDescription("Hit");
        event.setModelType("Model 3");
        event.setOem("Tesla");
        event.setId("1234");

        when(eventHistoryRepository.findById(anyString())).thenReturn(Optional.of(event));
        assertEquals(event, eventStoreService.getEvent("1234"));

    }

    @Test(expected = DataRetrievalFailureException.class)
    public void givenInvalidEventId_whenGetEvent_expectException() {
        when(eventHistoryRepository.findById(anyString())).thenReturn(Optional.empty());

        eventStoreService.getEvent("1234");
    }

    @Test
    public void givenValidEvents_whenGetEvents_expectList() {
        var event = new AVUpdateEvent();
        event.setCrashEventDescription("Hit");
        event.setModelType("Model 3");
        event.setOem("Tesla");
        event.setId("1234");

        when(eventHistoryRepository.findAll()).thenReturn(Arrays.asList(event, event));
        assertTrue(eventStoreService.getAllEvents().size() > 0);
    }

    @Test
    public void givenValidEvents_whenGetAllCrashes_expectListOnlyCrashes() {
        var event = new Event();
        event.setModelType("Model 3");
        event.setOem("Tesla");
        event.setId("1234");

        var crashEvent = new CrashEventDTO();
        crashEvent.setNearbyAvs(Arrays.asList("test", "test"));

        when(eventHistoryRepository.findAll()).thenReturn(Arrays.asList(event, event, crashEvent));
        assertEquals(1, eventStoreService.getAllCrashs().size());
    }

    @Test
    public void givenValidEvents_whenGetAllAvUpdates_expectListOnlyAVUpdateEvents() {
        var event = new Event();
        event.setModelType("Model 3");
        event.setOem("Tesla");
        event.setId("1234");

        var crashEvent = new CrashEventDTO();
        crashEvent.setNearbyAvs(Arrays.asList("test", "test"));

        var avUpdate = new AVUpdateEvent();
        avUpdate.setSpeed(124.0);

        when(eventHistoryRepository.findAll()).thenReturn(Arrays.asList(event, event, crashEvent, avUpdate));
        assertEquals(1, eventStoreService.getAllAVUpdates().size());
    }

    @Test(expected = DataRetrievalFailureException.class)
    public void givenInvalidEventId_whenDeleteEvent_expectException() {
        when(eventHistoryRepository.findById(anyString())).thenReturn(Optional.empty());

        eventStoreService.deleteEvent("1234");
    }


}

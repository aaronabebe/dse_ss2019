package at.tuwien.dse.eventstore.service;

import io.swagger.model.AccidentDTO;

/**
 * Service interface for accessing the EntityStore microservice.
 * */
public interface EntityStoreService {

    /**
     * Creates a new accident.
     * @param accidentDTO
     * */
    void createAccident(AccidentDTO accidentDTO);
}

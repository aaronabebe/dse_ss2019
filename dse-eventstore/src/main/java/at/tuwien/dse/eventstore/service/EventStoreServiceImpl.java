package at.tuwien.dse.eventstore.service;

import at.tuwien.dse.common.domain.AvPosition;
import at.tuwien.dse.common.messaging.producer.EventProducer;
import at.tuwien.dse.eventstore.repository.EventHistoryRepository;
import at.tuwien.dse.eventstore.repository.PositionRepository;
import io.swagger.model.*;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class EventStoreServiceImpl implements EventStoreService {

    private static final Logger logger = LoggerFactory.getLogger(EventStoreServiceImpl.class);

    private static final Long ACCIDENT_RADIUS_KM = 3L;

    private ModelMapper modelMapper;
    private EventProducer eventProducer;
    private EventHistoryRepository eventHistoryRepository;
    private EntityStoreService entityStoreService;
    private PositionRepository positionRepository;
    private SimpMessagingTemplate simpMessagingTemplate;

    public EventStoreServiceImpl(ModelMapper modelMapper,
                                 EventProducer eventProducer,
                                 EventHistoryRepository eventHistoryRepository,
                                 EntityStoreService entityStoreService,
                                 PositionRepository positionRepository,
                                 SimpMessagingTemplate simpMessagingTemplate) {
        this.modelMapper = modelMapper;
        this.eventProducer = eventProducer;
        this.eventHistoryRepository = eventHistoryRepository;
        this.entityStoreService = entityStoreService;
        this.positionRepository = positionRepository;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @Override
    public void processEvent(AVUpdateEvent avUpdateEvent) {
        AVUpdateEvent saved = eventHistoryRepository.save(avUpdateEvent);
        logger.info("Saved to Event-History: {}", saved);

        AvPosition avPosition = positionRepository.save(modelMapper.map(avUpdateEvent, AvPosition.class));
        logger.info("Saved Position: {}", avPosition);

        AvPositionDTO position = modelMapper.map(avPosition, AvPositionDTO.class);
        logger.info("Sending Position: {}", position);

        simpMessagingTemplate.convertAndSend("/topic/oems/" + avUpdateEvent.getOem() + "/avs/positions",
            position);

        if (Objects.nonNull(avUpdateEvent.getCrashEventDescription())) {
            CrashEventDTO crashEventDTO = modelMapper.map(avUpdateEvent, CrashEventDTO.class);
            crashEventDTO.setNearbyAvs(calculateNearbyAvs(crashEventDTO));

            eventProducer.sendCrashEvent(crashEventDTO);

            // TODO: add error handling when service inactive
            CrashEventDTO savedCrashEvent = eventHistoryRepository.save(crashEventDTO);
            entityStoreService.createAccident(new AccidentDTO()
                .status(AccidentStatus.ACTIVE)
                .event(savedCrashEvent));
        }
    }

    private List<String> calculateNearbyAvs(CrashEventDTO crashEventDTO) {
        return positionRepository.findAll().stream()
            .filter(avPosition -> isInRadius(ACCIDENT_RADIUS_KM, avPosition, crashEventDTO))
            .map(AvPosition::getChassisNumber)
            .collect(Collectors.toList());
    }

    private boolean isInRadius(Long radius, AvPosition avPosition, CrashEventDTO crashEventDTO) {
        //TODO check if working
        double dist = this.distance(avPosition.getLatitude(),
            crashEventDTO.getLatitude(),
            avPosition.getLongitude(),
            crashEventDTO.getLongitude(),
            300., 300.);
        return (dist / 1000.) < radius;
    }

    /**
     * TAKEN FROM: https://stackoverflow.com/questions/3694380/calculating-distance-between-two-points-using-latitude-longitude
     * Calculate distance between two points in latitude and longitude taking
     * into account height difference. If you are not interested in height
     * difference pass 0.0. Uses Haversine method as its base.
     * <p>
     * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters
     * el2 End altitude in meters
     *
     * @returns Distance in Meters
     */
    public static double distance(double lat1, double lat2, double lon1,
                                  double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
            + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
            * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }

    @Override
    public AVUpdateEvent getEvent(String eventId) {
        return (AVUpdateEvent) eventHistoryRepository.findById(eventId)
            .orElseThrow(() -> new DataRetrievalFailureException(String.format("Event %s not found", eventId)));
    }

    @Override
    public List<Event> getAllEvents() {
        return eventHistoryRepository.findAll();
    }

    @Override
    public List<CrashEventDTO> getAllCrashs() {
        return eventHistoryRepository.findAll()
            .stream()
            .filter(event -> event instanceof CrashEventDTO)
            .map(event -> (CrashEventDTO) event)
            .collect(Collectors.toList());
    }

    @Override
    public List<AVUpdateEvent> getAllAVUpdates() {
        return eventHistoryRepository.findAll()
            .stream()
            .filter(event -> event instanceof AVUpdateEvent)
            .map(event -> (AVUpdateEvent) event)
            .collect(Collectors.toList());
    }

    @Override
    public void deleteEvent(String eventId) {
        eventHistoryRepository.findById(eventId)
            .ifPresentOrElse(eventHistoryRepository::delete,
                () -> { throw new DataRetrievalFailureException(String.format("Event %s not found", eventId));});
    }

    @Override
    public List<AvPositionDTO> getCurrentPositions() {
        return positionRepository.findAll().stream()
            .map(position -> modelMapper.map(position, AvPositionDTO.class))
            .collect(Collectors.toList());
    }
}

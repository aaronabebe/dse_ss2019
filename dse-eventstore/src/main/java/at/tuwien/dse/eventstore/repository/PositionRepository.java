package at.tuwien.dse.eventstore.repository;

import at.tuwien.dse.common.domain.AvPosition;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PositionRepository  extends MongoRepository<AvPosition, String> {
}

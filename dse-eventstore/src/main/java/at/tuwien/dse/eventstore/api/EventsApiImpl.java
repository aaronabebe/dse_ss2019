package at.tuwien.dse.eventstore.api;

import at.tuwien.dse.eventstore.service.EventStoreService;
import io.swagger.api.EventsApi;
import io.swagger.model.AVUpdateEvent;
import io.swagger.model.AvPositionDTO;
import io.swagger.model.CrashEventDTO;
import io.swagger.model.Event;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/api")
@RestController
@CrossOrigin
public class EventsApiImpl implements EventsApi {

    private static final Logger logger = LoggerFactory.getLogger(EventsApiImpl.class);

    @Autowired
    private EventStoreService eventStoreService;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public ResponseEntity<Void> deleteEvent(String eventId) {
        eventStoreService.deleteEvent(eventId);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<List<AVUpdateEvent>> getAllAVUpdates() {
        return ResponseEntity.ok(eventStoreService.getAllAVUpdates());
    }

    @Override
    public ResponseEntity<List<CrashEventDTO>> getAllCrashs() {
        return ResponseEntity.ok(
            eventStoreService.getAllCrashs().stream()
                .map(event -> modelMapper.map(event, CrashEventDTO.class))
                .collect(Collectors.toList())
        );
    }

    @Override
    public ResponseEntity<List<Event>> getAllEvents() {
        return ResponseEntity.ok(eventStoreService.getAllEvents());
    }

    @Override
    public ResponseEntity<Event> getEvent(String eventId) {
        return ResponseEntity.ok(eventStoreService.getEvent(eventId));
    }

    @Override
    public ResponseEntity<List<AvPositionDTO>> getCurrentPositions() {
        return ResponseEntity.ok(eventStoreService.getCurrentPositions());
    }
}


package at.tuwien.dse.eventstore.service;

import at.tuwien.dse.common.messaging.consumer.UpdateEventConsumer;
import io.swagger.model.AVUpdateEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.header.Headers;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.StreamSupport;

@Service
public class UpdateEventService implements UpdateEventConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateEventService.class);

    @Autowired
    private EventStoreService eventStoreService;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void consumeUpdateEvent(ConsumerRecord<String, AVUpdateEvent> cr) {
        LOGGER.info("Message received: key {}: Type [{}] | Payload: {} | Record: {}", cr.key(),
            typeIdHeader(cr.headers()), cr.value(), cr.toString());
        eventStoreService.processEvent(cr.value());
    }

    private static String typeIdHeader(Headers headers) {
        return StreamSupport.stream(headers.spliterator(), false)
            .filter(header -> header.key().equals("__TypeId__"))
            .findFirst().map(header -> new String(header.value())).orElse("N/A");
    }

}
package at.tuwien.dse.eventstore.service;

import io.swagger.model.AccidentDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

@Service
public class EntityStoreServiceImpl implements EntityStoreService {

    @Value("${entitystore.uri}")
    private String ENTITY_STORE_URL;

    private static final Logger logger = LoggerFactory.getLogger(EntityStoreServiceImpl.class);
    private final RestTemplate restTemplate;

    public EntityStoreServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public void createAccident(AccidentDTO accidentDTO) {
        try {
            String result =
                restTemplate.postForObject(ENTITY_STORE_URL + "/entities/accident",
                    new HttpEntity<>(accidentDTO), String.class);

            logger.info("Sent: {} | To: {} | Received: {}", accidentDTO, ENTITY_STORE_URL, result);
        } catch (HttpStatusCodeException e){
            logger.error(e.getResponseBodyAsString());
        }
    }
}

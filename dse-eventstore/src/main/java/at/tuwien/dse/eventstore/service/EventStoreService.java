package at.tuwien.dse.eventstore.service;

import io.swagger.model.AVUpdateEvent;
import io.swagger.model.AvPositionDTO;
import io.swagger.model.CrashEventDTO;
import io.swagger.model.Event;

import java.util.List;

/**
 * This class handles incoming {@link AVUpdateEvent} and processes them.
 * It determines if they are {@link at.tuwien.dse.common.domain.CrashEvent} and sends them asynchronously
 * to the notificationstore for further processing.
 * */
public interface EventStoreService {


    /**
     * Accepts an incoming event and saves it to its data store. Also saves the current position of each car via
     * {@link at.tuwien.dse.eventstore.repository.PositionRepository}. Then the position data is sent to the frontend
     * via a websocket object.
     * If the event contains a crashEventDescription, it is classified as a {@link at.tuwien.dse.common.domain.CrashEvent}.
     * The nearby vehicles in a radius of 3km get calculated by their current position saved in the data store.
     * Then a new crashEvent gets produced for an asynchronuous message pipeline.
     * @param avUpdateEvent
     * */
    void processEvent(AVUpdateEvent avUpdateEvent);

    /**
     * Returns an event by its id.
     * @param eventId
     * @return AvUpdateEvent
     * */
    AVUpdateEvent getEvent(String eventId);

    /**
     * Returns a list of all events.
     * @return List<Event>
     * */
    List<Event> getAllEvents();

    /**
     * Returns a list of all Crashevents.
     * @return List<CrashEventDTO>
     * */
    List<CrashEventDTO> getAllCrashs();


    /**
     * Returns a list of all UpdateEvents
     * @return List<AvUpdateEvent>
     * */
    List<AVUpdateEvent> getAllAVUpdates();

    /**
     * Deletes an event by its id.
     * @param eventId
     * */
    void deleteEvent(String eventId);

    /**
     * Returns a list of the currentPositions of all currently driving AVs.
     * @return List<AvPositionDTO>
     * */
    List<AvPositionDTO> getCurrentPositions();
}

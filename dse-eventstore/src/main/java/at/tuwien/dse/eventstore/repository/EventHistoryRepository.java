package at.tuwien.dse.eventstore.repository;

import io.swagger.model.Event;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventHistoryRepository extends MongoRepository<Event, String> {
}

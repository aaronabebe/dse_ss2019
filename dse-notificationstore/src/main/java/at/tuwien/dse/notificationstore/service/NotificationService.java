package at.tuwien.dse.notificationstore.service;

import at.tuwien.dse.common.messaging.consumer.CrashEventConsumer;
import at.tuwien.dse.notificationstore.repository.NotificationRepository;
import io.swagger.model.*;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationService implements CrashEventConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationService.class);

    private static final String STOMP_CRASH_EVENT_ROUTE = "/topic/crash-event";   // /av/:id, /oem/:name, /ems

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private NotificationRepository repository;

    /**
     * Consumes incoming CrashEvents and then builds notifications for the different actors in the system.
     *
     * @param cr incoming consumer record
     */
    @Override
    public void consumeCrashEvent(ConsumerRecord<String, CrashEventDTO> cr) {
        LOGGER.info("Processing: {}", cr);

        CrashEventDTO crashEventDTO = cr.value();

        NotificationDTO oemNotification = new NotificationDTO()
            .notificationType(NotificationType.ACTIVATE_CRASH)
            .crashEventId(crashEventDTO.getId())
            .destination(String.format("/oem/%s", crashEventDTO.getOem()))
            .content(crashEventDTO);

        NotificationDTO emsNotification = new NotificationDTO()
            .notificationType(NotificationType.ACTIVATE_CRASH)
            .crashEventId(crashEventDTO.getId())
            .destination("/ems")
            .content(crashEventDTO);

        NotificationDTO avsNotification = new NotificationDTO()
            .notificationType(NotificationType.ACTIVATE_CRASH)
            .crashEventId(crashEventDTO.getId())
            .content((CrashEventDTO) (new CrashEventDTO()
                .latitude(crashEventDTO.getLatitude())
                .longitude(crashEventDTO.getLongitude())));

        LOGGER.info("oemNotification: {}", oemNotification);
        LOGGER.info("emsNotification: {}", emsNotification);
        LOGGER.info("avNotification:  {}", avsNotification);

        repository.save(oemNotification);
        repository.save(emsNotification);

        this.template.convertAndSend(STOMP_CRASH_EVENT_ROUTE + emsNotification.getDestination(), emsNotification);
        this.template.convertAndSend(STOMP_CRASH_EVENT_ROUTE + oemNotification.getDestination(), oemNotification);

        for (String avId : crashEventDTO.getNearbyAvs()) {
            avsNotification.destination("/av/" + avId);
            repository.save(avsNotification);
            this.template.convertAndSend(STOMP_CRASH_EVENT_ROUTE + avsNotification.getDestination(), avsNotification);
        }
    }

    /**
     * Sets a crash event to inactive and notifies clients.
     *
     * @param crashEventId the id of crash event
     */
    public void sendCrashEventInactive(String crashEventId) {
        List<NotificationDTO> notifications = repository
            .findByCrashEventIdAndAndNotificationType(crashEventId, NotificationType.ACTIVATE_CRASH);
        LOGGER.info("Setting crash-event-inactive notification...");

        for (NotificationDTO notification : notifications) {
            notification.notificationType(NotificationType.DEACTIVATE_CRASH);
            LOGGER.info("...to {}: {}", notification.getDestination(), notification);
            repository.save(notification);
            this.template.convertAndSend(STOMP_CRASH_EVENT_ROUTE + notification.getDestination(), notification);
        }


    }
}

package at.tuwien.dse.notificationstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@ComponentScan({"at.tuwien.dse.common.messaging.consumer",
    "at.tuwien.dse.common.configuration",
    "at.tuwien.dse.notificationstore"})
@EnableSwagger2
public class NotificationstoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(NotificationstoreApplication.class, args);
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage("at.tuwien.dse.notificationstore"))
            .build()
            .useDefaultResponseMessages(false).enableUrlTemplating(false);
    }

}

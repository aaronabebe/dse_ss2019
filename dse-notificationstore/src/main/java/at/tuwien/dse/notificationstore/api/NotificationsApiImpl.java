package at.tuwien.dse.notificationstore.api;

import at.tuwien.dse.notificationstore.service.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Rest Controller to update crash events.
 */
@RequestMapping("/api")
@RestController
public class NotificationsApiImpl implements io.swagger.api.NotificationsApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationsApiImpl.class);

    @Autowired
    private NotificationService notificationService;

    // better use rpc call -> grpc
    @PutMapping("/crash-event/{crashEventId}")
    public void sendCrashEventInactive(@PathVariable("crashEventId") String crashEventId){
        notificationService.sendCrashEventInactive(crashEventId);
    }
}

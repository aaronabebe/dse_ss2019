package at.tuwien.dse.notificationstore.repository;

import io.swagger.model.NotificationDTO;
import io.swagger.model.NotificationType;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository to save, edit and find crash events.
 */
@Repository
public interface NotificationRepository extends MongoRepository<NotificationDTO, String> {

    /**
     * Finds notification events by id and notification type.
     *
     * @param crashEventId the id of the event id
     * @param notificationType {@link NotificationType}
     * @return list of notification events
     */
    List<NotificationDTO> findByCrashEventIdAndAndNotificationType(String crashEventId, NotificationType notificationType);

}

package at.tuwien.dse.notificationstore.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;

@Component
public class StompConnectedEvent implements ApplicationListener<SessionConnectedEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(StompConnectedEvent.class);

    /**
     * Handle an application event.
     * @param event the event to respond to
     */
    @Override
    public void onApplicationEvent(SessionConnectedEvent event) {
        LOGGER.info("Client connected: {}", event);
    }
}

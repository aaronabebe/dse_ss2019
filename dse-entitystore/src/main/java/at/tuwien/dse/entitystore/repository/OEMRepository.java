package at.tuwien.dse.entitystore.repository;

import at.tuwien.dse.common.domain.OEM;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * MongoRepository for OEMs and AVs
 */
@Repository
public interface OEMRepository extends MongoRepository<OEM, String> {

    @Query(value = "{ '_id': ?0, 'avs._id' : ?1 }", fields = "{ 'avs.$' : 1 }")
    List<OEM> findByOEMNameAndAVId(String oemName, String chassisNumber);

}

package at.tuwien.dse.entitystore.service;

import at.tuwien.dse.common.domain.AV;
import at.tuwien.dse.common.domain.Accident;
import at.tuwien.dse.common.domain.OEM;
import at.tuwien.dse.entitystore.repository.AccidentRepository;
import at.tuwien.dse.entitystore.repository.OEMRepository;
import io.swagger.model.AccidentStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException; // not found
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;


/**
 * Service layer between endpoint and data access.
 * CRUD operations using the entitystore repositories.
 * Methods will throw DataRetrievalFailureException in case an entity could
 * not be retrieved and DuplicateKeyException if an entity already exists.
 */
@Service
public class EntitystoreService {
    private static final Logger LOGGER = LoggerFactory.getLogger(EntitystoreService.class);

    private final OEMRepository oemRepository;
    private final AccidentRepository accidentRepository;
    private final NotificationStoreService notificationStoreService;    // rest client api

    public EntitystoreService(OEMRepository oemRepository,
                              AccidentRepository accidentRepository,
                              NotificationStoreService notificationStoreService) {
        this.oemRepository = oemRepository;
        this.accidentRepository = accidentRepository;
        this.notificationStoreService = notificationStoreService;
    }

    public String createOEM(OEM oem) {
        OEM savedEntity = oemRepository.insert(oem);
        LOGGER.debug("Saved: {}", savedEntity);
        return savedEntity.getName();
    }

    public String createAV(String oemName, AV av) {
        OEM oem = this.getOEM(oemName);
        if (oem == null) {
            throw new DataRetrievalFailureException(String.format("OEM %s not found", oemName));
        }
        if (!oem.addAV(av)) {
            throw new DuplicateKeyException(String.format("AV %s already exists", av.getChassisNumber()));
        }
        oem = oemRepository.save(oem);
        LOGGER.debug("Saved: {}", oem);
        return av.getChassisNumber();
    }

    public String createAccident(Accident accident) {
        Accident savedEntity = accidentRepository.insert(accident);
        LOGGER.debug("Saved: {}", savedEntity);
        return savedEntity.getId();
    }


    public OEM getOEM(String oemName) {
        Optional<OEM> oem = oemRepository.findById(oemName);
        if (oem.isEmpty())
            throw new DataRetrievalFailureException(String.format("OEM %s not found", oemName));
        LOGGER.debug("GOT: {}", oem.get());
        return oem.get();
    }

    public List<OEM> getOEMs() {
        return oemRepository.findAll();
    }

    public AV getAV(String oemName, String chassisNumber) {
        List<OEM> oems = oemRepository.findByOEMNameAndAVId(oemName, chassisNumber);
        LOGGER.debug("{}", oems);
        if (oems.isEmpty()) {
            throw new DataRetrievalFailureException(String.format("OEM %s not found", oemName));
        }
        if (oems.get(0).getAV(chassisNumber) == null) {
            throw new DataRetrievalFailureException(String.format("AV %s not found", chassisNumber));
        }
        return oems.get(0).getAV(chassisNumber);
    }

    public List<AV> getAVs(String oemName) {
        return this.getOEM(oemName).getAVs();
    }

    public Accident getAccident(String id) {
        Optional<Accident> accident = accidentRepository.findById(id);
        if (accident.isEmpty()) {
            throw new DataRetrievalFailureException(String.format("Accident %s not found", id));
        }
        return accident.get();
    }

    public List<Accident> getAccidents() {
        return accidentRepository.findAll();
    }

    public List<Accident> getAccidents(AccidentStatus status) {
        return accidentRepository.findAccidentsByStatus(status);
    }

    public String updateAV(String oemName, AV av) {
        AV avFound = this.getAV(oemName, av.getChassisNumber());
        if (avFound == null) {
            throw new DataRetrievalFailureException(String.format("AV %s not found", av.getChassisNumber()));
        }
        this.removeAV(oemName, av.getChassisNumber());
        return this.createAV(oemName, av);
    }

    public String updateAccident(Accident accident) {
        Accident loadedEntity = this.getAccident(accident.getId());
        Accident savedEntity = accidentRepository.save(accident);

        // in case the status has changed from active to inactive, notify
        // the notificationstoreservice to send notifications to the UI
        if (loadedEntity.getStatus().equals(AccidentStatus.ACTIVE) &&
            savedEntity.getStatus().equals(AccidentStatus.INACTIVE)) {
            notificationStoreService.setCrashEventInactive(loadedEntity.getEvent().getId());
        }

        return savedEntity.getId();
    }

    public void removeOEM(String oemName) {
        this.getOEM(oemName);
        oemRepository.deleteById(oemName);
    }

    public void removeAV(String oemName, String chassisNumber) {
        OEM oem = this.getOEM(oemName);
        LOGGER.debug("{}", oem);
        if (oem.getAV(chassisNumber) == null) {
            throw new DataRetrievalFailureException(String.format("AV %s not found", chassisNumber));
        }
        oem.removeAV(chassisNumber);
        oemRepository.save(oem);
    }

    public void removeAccident(String id) {
        this.getAccident(id);
        accidentRepository.deleteById(id);
    }


}

package at.tuwien.dse.entitystore.repository;

import at.tuwien.dse.common.domain.Accident;
import io.swagger.model.AccidentStatus;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * MongoRepository for the accidents
 */
@Repository
public interface AccidentRepository extends MongoRepository<Accident, String> {

    List<Accident> findAccidentsByStatus(AccidentStatus status);

}

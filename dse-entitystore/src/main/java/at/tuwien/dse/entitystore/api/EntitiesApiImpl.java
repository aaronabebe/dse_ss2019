package at.tuwien.dse.entitystore.api;

import at.tuwien.dse.common.domain.AV;
import at.tuwien.dse.common.domain.Accident;
import at.tuwien.dse.common.domain.OEM;
import at.tuwien.dse.entitystore.service.EntitystoreService;
import io.swagger.api.EntitiesApi;
import io.swagger.model.AVDTO;
import io.swagger.model.AccidentDTO;
import io.swagger.model.AccidentStatus;
import io.swagger.model.OEMDTO;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.List;

@RestController
@RequestMapping("/api")
@Validated
public class EntitiesApiImpl implements EntitiesApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(EntitiesApiImpl.class);
    private EntitystoreService entitystoreService;
    private ModelMapper modelMapper;

    public EntitiesApiImpl(EntitystoreService entitystoreService, ModelMapper modelMapper) {
        this.entitystoreService = entitystoreService;
        this.modelMapper = modelMapper;
    }

    @Override
    public ResponseEntity<String> addOEM(@Valid OEMDTO body) {
        OEM oem = new OEM();
        modelMapper.map(body, oem);
        String name = entitystoreService.createOEM(oem);
        return ResponseEntity.ok(name);
    }

    @Override
    public ResponseEntity<String> addAV(@Valid AVDTO body, String oeMName) {
        AV av = new AV();
        modelMapper.map(body, av);
        String id = entitystoreService.createAV(oeMName, av);
        return ResponseEntity.ok(id);
    }

    @Override
    public ResponseEntity<String> addAccident(@Valid AccidentDTO body) {
        Accident accident = new Accident();
        modelMapper.map(body, accident);
        String id = entitystoreService.createAccident(accident);
        return ResponseEntity.ok(id);
    }

    @Override
    public ResponseEntity<Void> deleteAV(String oeMName, String avId) {
        entitystoreService.removeAV(oeMName, avId);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<Void> deleteAccident(String id) {
        entitystoreService.removeAccident(id);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<Void> deleteOEM(String oeMName) {
        entitystoreService.removeOEM(oeMName);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<AVDTO> getAV(String oeMName, String avId) {
        AV av = entitystoreService.getAV(oeMName, avId);
        AVDTO avdto = new AVDTO();
        modelMapper.map(av, avdto);
        return ResponseEntity.ok(avdto);
    }

    @Override
    public ResponseEntity<List<AVDTO>> getAVs(String oeMName) {
        List<AV> avs = entitystoreService.getAVs(oeMName);
        Type listType = new TypeToken<List<AVDTO>>() {
        }.getType();
        List<AVDTO> avdtos = modelMapper.map(avs, listType);
        return ResponseEntity.ok(avdtos);
    }

    @Override
    public ResponseEntity<AccidentDTO> getAccident(String id) {
        Accident accident = entitystoreService.getAccident(id);
        AccidentDTO accidentDTO = new AccidentDTO();
        modelMapper.map(accident, accidentDTO);
        return ResponseEntity.ok(accidentDTO);
    }

    @Override
    public ResponseEntity<List<AccidentDTO>> getAccidents() {
        List<Accident> accidents = entitystoreService.getAccidents();
        Type listType = new TypeToken<List<AccidentDTO>>() {
        }.getType();
        List<AccidentDTO> accidentDTOS = modelMapper.map(accidents, listType);
        return ResponseEntity.ok(accidentDTOS);
    }

    @Override
    public ResponseEntity<List<AccidentDTO>> getAccidentsByStatus(AccidentStatus status) {
        List<Accident> accidents = entitystoreService.getAccidents(status);
        Type listType = new TypeToken<List<AccidentDTO>>() {
        }.getType();
        List<AccidentDTO> accidentDTOS = modelMapper.map(accidents, listType);
        return ResponseEntity.ok(accidentDTOS);
    }

    @Override
    public ResponseEntity<OEMDTO> getOEM(String oeMName) {
        OEM oem = entitystoreService.getOEM(oeMName);
        OEMDTO oemdto = new OEMDTO();
        modelMapper.map(oem, oemdto);
        return ResponseEntity.ok(oemdto);
    }

    @Override
    public ResponseEntity<List<OEMDTO>> getOEMs() {
        List<OEM> oems = entitystoreService.getOEMs();
        Type listType = new TypeToken<List<OEMDTO>>() {
        }.getType();
        List<OEMDTO> oemdtos = modelMapper.map(oems, listType);
        return ResponseEntity.ok(oemdtos);
    }

    @Override
    public ResponseEntity<Void> updateAV(@Valid AVDTO body, String oeMName) {
        AV av = new AV();
        modelMapper.map(body, av);
        entitystoreService.updateAV(oeMName, av);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<Void> setAccidentInactive(String id) {
        Accident accident = entitystoreService.getAccident(id);
        accident.setStatus(AccidentStatus.INACTIVE);
        entitystoreService.updateAccident(accident);
        return ResponseEntity.noContent().build();
    }


//    @Override
//    public ResponseEntity<String> addAV(@Valid AVDTO body, String oeMName) {
//        if (!body.getOem().getName().equals(oeMName))
//            return ResponseEntity.status(405).build();
//
//        if (entitystoreService.getOEM(oeMName) == null)
//            return ResponseEntity.notFound().build();
//
//        String avId = entitystoreService.createAV(body);
//        return ResponseEntity.ok(avId);
//    }

}

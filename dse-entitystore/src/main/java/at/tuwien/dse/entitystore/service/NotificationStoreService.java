package at.tuwien.dse.entitystore.service;

/**
 * NotificationStoreService client implementation
 */
public interface NotificationStoreService {
    void setCrashEventInactive(String crashEventId);
}

package at.tuwien.dse.entitystore.interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;


/**
 * Exception interceptor handles DuplicateKeyException and DataRetrievalFailureExceptions
 * thrown in the {@link at.tuwien.dse.entitystore.service.EntitystoreService} and
 * translates them to HTTP status responses
 */
@ControllerAdvice
public class ExceptionInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionInterceptor.class);

    @ResponseStatus(HttpStatus.CONFLICT)    // 409
    @ExceptionHandler(DuplicateKeyException.class)
    public void handleAlreadyExists(DuplicateKeyException ex) {
        LOGGER.debug(ex.getMessage());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)   // 404
    @ExceptionHandler(DataRetrievalFailureException.class)
    public void handleNotFound(DataRetrievalFailureException ex) {
        LOGGER.debug(ex.getMessage());
    }

}

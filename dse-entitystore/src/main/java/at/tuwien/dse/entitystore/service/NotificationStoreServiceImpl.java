package at.tuwien.dse.entitystore.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;


@Service
public class NotificationStoreServiceImpl implements NotificationStoreService {
    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationStoreServiceImpl.class);

    @Value("${notificationstore.uri}")
    private String NOTIFICATION_STORE_URI;

    @Autowired
    private RestTemplate restTemplate;


    @Override
    public void setCrashEventInactive(String crashEventId) {
        try {
            restTemplate.put(String.format("%s/api/crash-event/%s", NOTIFICATION_STORE_URI, crashEventId), null);
            LOGGER.info("Sent: set inactive {} | To: {}", crashEventId, NOTIFICATION_STORE_URI);
        } catch (HttpStatusCodeException e){
            LOGGER.error(e.getResponseBodyAsString());
        }
    }
}

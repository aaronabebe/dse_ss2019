package at.tuwien.dse.entitystore.api;

import at.tuwien.dse.common.domain.AV;
import at.tuwien.dse.common.domain.Accident;
import at.tuwien.dse.entitystore.repository.AccidentRepository;
import at.tuwien.dse.entitystore.repository.OEMRepository;
import at.tuwien.dse.entitystore.service.EntitystoreService;
import at.tuwien.dse.entitystore.service.NotificationStoreService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.model.AVDTO;
import io.swagger.model.AccidentDTO;
import io.swagger.model.AccidentStatus;
import io.swagger.model.CrashEventDTO;
import io.swagger.model.OEMDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = EntitiesApiImpl.class)
@AutoConfigureWebClient
public class EntitiesApiTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OEMRepository oemRepository;

    @MockBean
    private AccidentRepository accidentRepository;

    @MockBean
    private NotificationStoreService notificationStoreService;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private EntitystoreService entitystoreService;

    @Test
    public void givenEmptyBody_whenCreateOEM_expect400() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/oem").build();

        mockMvc.perform(post(path.toString())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void givenBodyWithMissingMandatoryParams_whenCreateOEM_expect400() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/oem").build();

        var oemdto = new OEMDTO();
        oemdto.setAvs(Collections.singletonList(new AVDTO()));

        mockMvc.perform(post(path.toString())
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(oemdto)))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void givenValidBody_whenCreateOEM_expect200() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/oem").build();

        var oemdto = new OEMDTO();
        oemdto.setName("Tesla");

        mockMvc.perform(post(path.toString())
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(oemdto)))
            .andExpect(status().isOk());
    }

    @Test
    public void givenEmptyBody_whenCreateAV_expect400() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/oem/{OEMName}/av").build()
            .expand("Tesla");

        mockMvc.perform(post(path.toString())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void givenBodyWithMissingMandatoryParams_whenCreateAV_expect400() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/oem/{OEMName}/av").build()
            .expand("Tesla");

        var avdto = new AVDTO();
        avdto.setModelType("Model 3");

        mockMvc.perform(post(path.toString())
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(avdto)))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void givenValidBody_whenCreateAV_expect200() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/oem/{OEMName}/av").build()
            .expand("Tesla");

       var avdto = new AVDTO();
       avdto.setModelType("Model 3");
       avdto.setChassisNumber("1234435");

        mockMvc.perform(post(path.toString())
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(avdto)))
            .andExpect(status().isOk());
    }

    @Test
    public void givenValidBodyWithUnkownOEM_whenCreateAV_expect404() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/oem/{OEMName}/av").build()
            .expand("Tesla");

        when(entitystoreService.createAV(anyString(), any())).thenThrow(DataRetrievalFailureException.class);

        var avdto = new AVDTO();
        avdto.setModelType("Model 3");
        avdto.setChassisNumber("12352145");

        mockMvc.perform(post(path.toString())
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(avdto)))
            .andExpect(status().isNotFound());
    }

    @Test
    public void givenValidBodyWithExistingOEM_whenCreateAV_expect409() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/oem/{OEMName}/av").build()
            .expand("Tesla");

        when(entitystoreService.createAV(anyString(), any())).thenThrow(DuplicateKeyException.class);

        var avdto = new AVDTO();
        avdto.setModelType("Model 3");
        avdto.setChassisNumber("1123534");

        mockMvc.perform(post(path.toString())
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(avdto)))
            .andExpect(status().isConflict());
    }

    @Test
    public void givenEmptyBody_whenCreateAccident_expect400() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/accident").build();

        mockMvc.perform(post(path.toString())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void givenBodyWithMissingMandatoryParams_whenCreateAccident_expect400() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/accident").build();

        var accident = new AccidentDTO();
        accident.setStatus(AccidentStatus.ACTIVE);

        mockMvc.perform(post(path.toString())
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(accident)))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void givenValidBody_whenCreateAccident_expect200() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/accident").build();

        var accident = new AccidentDTO();
        accident.setStatus(AccidentStatus.ACTIVE);
        accident.setEvent(new CrashEventDTO());

        mockMvc.perform(post(path.toString())
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(accident)))
            .andExpect(status().isOk());
    }

    @Test
    public void givenKnownOEMandAV_whenDeleteAV_expect204() throws Exception{
        var path = UriComponentsBuilder.fromPath("/api/entities/oem/{OEMName}/av/{AVId}")
            .buildAndExpand("Tesla", "123432");

        doNothing().when(entitystoreService).removeAV(anyString(), anyString());
        mockMvc.perform(delete(path.toString()))
            .andExpect(status().isNoContent());
        
    }

    @Test
    public void givenUnknownOEMandAV_whenDeleteAV_expect404() throws Exception{
        var path = UriComponentsBuilder.fromPath("/api/entities/oem/{OEMName}/av/{AVId}")
            .buildAndExpand("Tesla", "123432");

       doThrow(DataRetrievalFailureException.class).when(entitystoreService).removeAV(anyString(), anyString());

        mockMvc.perform(delete(path.toString()))
            .andExpect(status().isNotFound());

    }

    @Test
    public void givenKnownId_whenDeleteAccident_expect204() throws Exception{
        var path = UriComponentsBuilder.fromPath("/api/entities/accident/{id}")
            .buildAndExpand("123432");

        doNothing().when(entitystoreService).removeAccident(anyString());
        mockMvc.perform(delete(path.toString()))
            .andExpect(status().isNoContent());

    }

    @Test
    public void givenKnownOEM_whenDeleteAccident_expect204() throws Exception{
        var path = UriComponentsBuilder.fromPath("/api/entities/oem/{OEMName}")
            .buildAndExpand("Tesla");

        doNothing().when(entitystoreService).removeOEM(anyString());
        mockMvc.perform(delete(path.toString()))
            .andExpect(status().isNoContent());

    }

    @Test
    public void givenKnownOEMandId_whenGetAV_expectJsonResult() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/oem/{OEMName}/av/{AVId}")
            .buildAndExpand("Tesla", "1232345");

        var av = new AV();
        av.setChassisNumber("123512");
        av.setModelType("Model 3");

        when(entitystoreService.getAV(anyString(), anyString())).thenReturn(av);
        mockMvc.perform(get(path.toString())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("chassisNumber").value(av.getChassisNumber()))
            .andExpect(jsonPath("modelType").value(av.getModelType()));
    }

    @Test
    public void givenUnknownOEMandId_whenGetAV_expect404() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/oem/{OEMName}/av/{AVId}")
            .buildAndExpand("Tesla", "1232345");

        var av = new AV();
        av.setChassisNumber("123512");
        av.setModelType("Model 3");

        when(entitystoreService.getAV(anyString(), anyString())).thenThrow(DataRetrievalFailureException.class);
        mockMvc.perform(get(path.toString()))
            .andExpect(status().isNotFound());
    }

    @Test
    public void givenKnownOEMandId_whenGetAVs_expectJsonArray() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/oem/{OEMName}/avs")
            .buildAndExpand("Tesla");

        var av = new AV();
        av.setChassisNumber("123512");
        av.setModelType("Model 3");

        when(entitystoreService.getAVs(anyString())).thenReturn(Arrays.asList(av, av));
        mockMvc.perform(get(path.toString())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("[0].chassisNumber").value(av.getChassisNumber()))
            .andExpect(jsonPath("[0].modelType").value(av.getModelType()));
    }

    @Test
    public void givenKnownId_whenGetAccident_expectJsonResult() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/accident/{id}")
            .buildAndExpand( "1232345");

        var accident = new Accident();
        accident.setDescription("Test accident");
        accident.setStatus(AccidentStatus.ACTIVE);
        accident.setEvent(new CrashEventDTO());

        when(entitystoreService.getAccident(anyString())).thenReturn(accident);
        mockMvc.perform(get(path.toString())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("status").value(accident.getStatus().toString()));
    }

    @Test
    public void givenUnknownId_whenGetAccident_expect404() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/accident/{id}")
            .buildAndExpand("Tesla", "1232345");

        when(entitystoreService.getAccident(anyString())).thenThrow(DataRetrievalFailureException.class);
        mockMvc.perform(get(path.toString()))
            .andExpect(status().isNotFound());
    }

    @Test
    public void givenKnownAccident_whenGetAccidents_expectJsonArray() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/accidents")
            .buildAndExpand("Tesla");

        var accident = new Accident();
        accident.setDescription("Test accident");
        accident.setStatus(AccidentStatus.ACTIVE);
        accident.setEvent(new CrashEventDTO());

        when(entitystoreService.getAccidents()).thenReturn(Arrays.asList(accident, accident));
        mockMvc.perform(get(path.toString())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("[0].status").value(accident.getStatus().toString()));
    }

    @Test
    public void givenValidAvIdAndEmptyBody_whenEditAv_expect400() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/oem/{OEMName}/av")
            .buildAndExpand("Tesla");

        mockMvc.perform(put(path.toString())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void givenValidAvIdAndMissingMandatoryParam_whenEditAv_expect400() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/oem/{OEMName}/av")
            .buildAndExpand("Tesla");

        var av = new AVDTO();
        av.setModelType("1244");

        mockMvc.perform(put(path.toString())
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(av)))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void givenValidAvIdAndBody_whenEditAv_expect204() throws Exception {
        var path = UriComponentsBuilder.fromPath("/api/entities/oem/{OEMName}/av")
            .buildAndExpand("Tesla");

        var av = new AVDTO();
        av.setModelType("Model 3");
        av.setChassisNumber("113234");

        when(entitystoreService.updateAV(anyString(), any())).thenReturn("123234");
        mockMvc.perform(put(path.toString())
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(av)))
            .andExpect(status().isNoContent());
    }

    @Test
    public void givenValidBody_whenEditAccident_expect204() throws Exception {
        when(entitystoreService.updateAccident(any())).thenReturn("id");
        when(entitystoreService.getAccident(any())).thenReturn(new Accident());
        mockMvc.perform(put("/api/entities/accident/1")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());
    }
}

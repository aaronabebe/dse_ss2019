package at.tuwien.dse.entitystore.service;

import at.tuwien.dse.common.domain.AV;
import at.tuwien.dse.common.domain.Accident;
import at.tuwien.dse.common.domain.OEM;
import at.tuwien.dse.entitystore.repository.AccidentRepository;
import at.tuwien.dse.entitystore.repository.OEMRepository;
import io.swagger.model.AccidentStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EntitystoreServiceTests {

    @Autowired
    private OEMRepository oemRepository;

    @Autowired
    private AccidentRepository accidentRepository;

    @Autowired
    private EntitystoreService entitystoreService;

    @Before
    public void before() {
        oemRepository.deleteAll();
        accidentRepository.deleteAll();
    }

    @After
    public void after() {
        oemRepository.deleteAll();
        accidentRepository.deleteAll();
    }

    @Test
    public void testCreateOEM() {
        OEM oem = new OEM("Audi");
        entitystoreService.createOEM(oem);
        assertTrue(oemRepository.findById("Audi").isPresent());
    }

    @Test(expected = DuplicateKeyException.class)
    public void testCreateOEM_DuplicateEntry() {
        entitystoreService.createOEM(new OEM("Audi"));
        entitystoreService.createOEM(new OEM("Audi"));
    }

    @Test(expected = DataRetrievalFailureException.class)
    public void testGetOEM_NonExistent() {
        entitystoreService.getOEM("abc");
    }

    @Test
    public void testUpdateAV() {
        OEM oem1 = new OEM("oem1");
        oem1.addAV(new AV("kombi1", "123", null));
        entitystoreService.createOEM(oem1);
        assertEquals("kombi1", entitystoreService.getAV("oem1", "123").getModelType());
        entitystoreService.updateAV("oem1", new AV("suv", "123", null));
        assertEquals("suv", entitystoreService.getAV("oem1", "123").getModelType());
    }

    @Test
    public void testUpdateAccident() {
        Accident acc1 = new Accident("An accident occurred", AccidentStatus.ACTIVE, null);
        String id = entitystoreService.createAccident(acc1);
        assertTrue(accidentRepository.findById(id).isPresent());
        acc1.setDescription("Nevermind");
        entitystoreService.updateAccident(acc1);
        assertEquals(1, accidentRepository.findAll().size());
        assertEquals("Nevermind", accidentRepository.findById(id).get().getDescription());
    }

    @Test
    public void testRemoveOEM() {
        entitystoreService.createOEM(new OEM("oem1"));
        entitystoreService.createOEM(new OEM("oem2"));
        entitystoreService.createOEM(new OEM("oem3"));
        entitystoreService.removeOEM("oem1");
        assertEquals(2, oemRepository.findAll().size());
    }

    @Test(expected = DataRetrievalFailureException.class)
    public void testGetAV() {
        OEM oem1 = new OEM("oem1");
        OEM oem2 = new OEM("oem2");
        oem1.addAV(new AV("kombi1", "123", null));
        oem1.addAV(new AV("kombi2", "666", null));
        oem2.addAV(new AV("kombi1", "123", null));
        entitystoreService.createOEM(oem1);
        entitystoreService.createOEM(oem2);
        assertEquals( "123", entitystoreService.getAV("oem1", "123").getChassisNumber());
        assertEquals( "666", entitystoreService.getAV("oem1", "666").getChassisNumber());
        assertEquals( "123", entitystoreService.getAV("oem2", "123").getChassisNumber());
        assertNull(entitystoreService.getAV("oem2", "666")); // throws
    }

    @Test
    public void testRemoveAV() {
        OEM oem1 = new OEM("oem1");
        OEM oem2 = new OEM("oem2");
        oem1.addAV(new AV("kombi1", "123", null));
        oem1.addAV(new AV("kombi2", "666", null));
        oem2.addAV(new AV("kombi1", "123", null));
        entitystoreService.createOEM(oem1);
        entitystoreService.createOEM(oem2);
        entitystoreService.removeAV("oem1", "666");
        assertEquals(1, oemRepository.findById("oem1").get().getAVs().size());
        entitystoreService.removeAV("oem1", "123");
        assertTrue(oemRepository.findById("oem1").get().getAVs().isEmpty());
    }

    @Test
    public void testCreateAccident() {
        Accident acc1 = new Accident("Ein Unfall", AccidentStatus.ACTIVE, null);
        String id = entitystoreService.createAccident(acc1);
        assertTrue(accidentRepository.findById(id).isPresent());
    }

    @Test
    public void testRemoveAccident() {
        Accident acc1 = new Accident("Ein Unfall", AccidentStatus.ACTIVE, null);
        String id = entitystoreService.createAccident(acc1);
        entitystoreService.removeAccident(id);
        assertTrue(accidentRepository.findAll().isEmpty());
    }


}
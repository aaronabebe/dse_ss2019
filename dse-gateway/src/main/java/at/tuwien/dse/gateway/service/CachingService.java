package at.tuwien.dse.gateway.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



/**
 * This class caches all incoming requests with the route as key and the payload as value.
 * */
@Service
public class CachingService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CachingService.class);

    @Autowired
    private CacheHelper cacheHelper;

    public void cache(String path, String s) {
        LOGGER.info("Caching {}: {}", path, s);
        cacheHelper.getRoutesCacheFromCacheManager().put(path, s);
    }

    public String getCacheEntry(String key) {
        if (cacheHelper.getRoutesCacheFromCacheManager().containsKey(key))
            return cacheHelper.getRoutesCacheFromCacheManager().get(key);
        return null;
    }

    public void refresh() {
        // TODO: implement automated timerbased refreshing of all routes
        LOGGER.info("Refreshing route cache");
//        routeLocator.getRoutes().all(route -> {
//            LOGGER.warn("ROUTE: {}", route.getUri());
//            return true;
//        });
    }
}

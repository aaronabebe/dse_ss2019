package at.tuwien.dse.gateway.api;

import at.tuwien.dse.common.messaging.producer.EventProducer;
import io.swagger.model.AVUpdateEvent;
import io.swagger.model.CrashEventDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ThreadLocalRandom;

@RestController
public class EventQueueApi {

    @Autowired
    private EventProducer eventProducer;

    @PostMapping("/generator/start")
    public void startDataGeneration() {
        TimerTask repeatedTask = new TimerTask() {
            public void run() {
                eventProducer.sendUpdateEvent(createUpdateEvent("123"));
            }
        };
        Timer timer = new Timer("Timer");
        long delay = 5000L;
        long period = 5000L;
        timer.scheduleAtFixedRate(repeatedTask, delay, period);
    }

    private AVUpdateEvent createUpdateEvent(String chassisNumber) {
        return (AVUpdateEvent) new AVUpdateEvent()
            .speed(10.0)
            .sequenceNumber(123)
            .oem("Audi")
            .crashEventDescription("hit an obstacle")
            .modelType("A8")
            .chassisNumber(chassisNumber)
            .latitude(ThreadLocalRandom.current().nextDouble(50, 51))
            .longitude(ThreadLocalRandom.current().nextDouble(20, 21))
            .numberOfOccupants(2);

    }

    @GetMapping("/update/{chassisNumber}")
    public void sendUpdate(@PathVariable String chassisNumber) {
        eventProducer.sendUpdateEvent(createUpdateEvent(chassisNumber));
    }

    @PostMapping(value = "/av/update", consumes = {"application/json"})
    public void avUpdate(@Valid @RequestBody AVUpdateEvent body) {
        eventProducer.sendUpdateEvent(body);
    }

    @PostMapping(value = "/crash", consumes = {"application/json"})
    public void crashEvent(@Valid @RequestBody CrashEventDTO body) {
        eventProducer.sendCrashEvent(body);
    }
}

package at.tuwien.dse.gateway.routing;

import at.tuwien.dse.gateway.service.CachingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.LinkedHashSet;



/**
 * Configuration class for redirecting outside calls to the internal microservices.
 *
 * */
@Configuration
public class GatewayRoutes {
    private static final Logger LOGGER = LoggerFactory.getLogger(GatewayRoutes.class);

    @Value("${entitystore.uri}")
    private String ENTITY_STORE_URI;
    @Value("${eventstore.uri}")
    private String EVENT_STORE_URI;
    @Value("${notificationstore.uri}")
    private String NOTIFICATION_STORE_URI;

    private final String ENTITY_STORE_PREFIX = "/api/entities";
    private final String EVENT_STORE_PREFIX = "/api/events";
    private final String NOTIFICATION_STORE_PREFIX = "/api/notifications";

    @Autowired
    private CachingService cachingService;


    /**
     * Matches a path by predefined patterns and then edits the url to redirect to the right microservice-endpoint.
     * For requests to the entitystore a caching mechanism is used.
     * @param builder
     * */
    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder builder) {
        LOGGER.info(ENTITY_STORE_URI);
        return builder.routes()
            .route("entitystore-route",
                r -> r.path("/accident*/**", "/oem*/**")
                    .filters(f -> f
                        .prefixPath(ENTITY_STORE_PREFIX)
                        .modifyResponseBody(String.class, String.class, (exchange, s) -> {

                            if (exchange.getResponse().getStatusCode().is3xxRedirection()) {
                                return Mono.just(s);   // return cache fallback response
                            }
                            if (exchange.getRequest().getMethodValue().equals("GET")) {  // cache only get methods
                                cachingService.cache(exchange.getRequest().getPath().toString(), s);
                            }
                            return Mono.just(s);
                        })
                        .hystrix(config -> config
                            .setName("entitystore-fallback")
                            .setFallbackUri("forward:/fallback")))
                    .uri(ENTITY_STORE_URI))
            .route("notifications-websocket-route",
                r -> r
                    .path("/notifications**")
                    .uri(NOTIFICATION_STORE_URI))
            .route("events-websocket-route",
                r -> r
                    .path("/ws/events**")
                    .uri(EVENT_STORE_URI))
            .route("eventstore-route",
                r -> r
                    .path("/events/**")
                    .filters(f -> f.prefixPath(EVENT_STORE_PREFIX))
                    // TODO: use caching service as well
                    .uri(EVENT_STORE_URI))
            .build();
    }

    /**
     * A global filter to add a custom header to each request.
     * The header is used to identify the original path of the request after redirecting for using caching and fallbacks.
     * */
    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public GlobalFilter addRequestHeaderXOrigin() {
        return (exchange, chain) -> {
            try {
                LinkedHashSet uriSet = exchange.getAttribute(ServerWebExchangeUtils.GATEWAY_ORIGINAL_REQUEST_URL_ATTR);
                URI uri = (URI) uriSet.toArray()[0];
                exchange.getRequest().mutate().header("X-OriginalRequestURL", uri.getPath()).build();
                LOGGER.debug("first pre filter {}", uri);
            } catch (Exception e) {
                LOGGER.error("addRequestHeaderXOrigin failed: {}", e.getMessage());
            }
            return chain.filter(exchange).then();
        };
    }
}

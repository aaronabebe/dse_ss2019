package at.tuwien.dse.gateway.api;

import at.tuwien.dse.gateway.service.CachingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class FallbackApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(FallbackApi.class);

    @Autowired
    private CachingService cachingService;

    @RequestMapping(value = "/fallback", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity fallback(@RequestHeader HttpHeaders httpHeaders) {
        LOGGER.debug("HEADERS: {}", httpHeaders);
        String cacheEntry = null;
        try {
            String path = httpHeaders.get("X-OriginalRequestURL").get(0);
            cacheEntry = cachingService.getCacheEntry(path);
            LOGGER.info("Using cache fallback: {}", cacheEntry);
        } catch (NullPointerException e) {
            LOGGER.error(e.getMessage());
        }

        if (cacheEntry == null){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.status(HttpStatus.TEMPORARY_REDIRECT).body(cacheEntry);
    }

    @RequestMapping(value = "/fallback", method = {RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT})
    public ResponseEntity fallbackUnavailableForMethods(){
        return ResponseEntity.notFound().build();
    }

//    @GetMapping(value = "/refresh")
//    public ResponseEntity refresh(){
//        cachingService.refresh();
//        return ResponseEntity.ok("");
//    }
}

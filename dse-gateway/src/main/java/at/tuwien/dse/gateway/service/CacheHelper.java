package at.tuwien.dse.gateway.service;

import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.springframework.stereotype.Component;


/**
 * Helper class for the cache.
 * */
@Component
public class CacheHelper {

    private CacheManager cacheManager;
    private Cache<String, String> routesCache;

    public CacheHelper() {
        cacheManager = CacheManagerBuilder
            .newCacheManagerBuilder().build();
        cacheManager.init();
        routesCache = cacheManager.createCache("routes",
            CacheConfigurationBuilder.newCacheConfigurationBuilder(
                String.class,
                String.class,
                ResourcePoolsBuilder.heap(100)));
    }

    /**
     * Retrieves all cached instances from the cache.
     * */
    public Cache<String, String> getRoutesCacheFromCacheManager() {
        return cacheManager.getCache("routes", String.class, String.class);
    }
}
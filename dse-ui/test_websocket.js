var Stomp = require('stompjs');

// var SockJS = require('sockjs-client');
// var sock = new SockJS('http://localhost:9093/notifications');
// var client = Stomp.over(sock);

var client = Stomp.overWS('ws://localhost:9000/notifications');

var connect_callback = function () {
    console.log('connected');
};

var error_callback = function (e) {
    console.log('ERROR: ' + e);
};

var message_callback_av1 = function (message) {
    // called when the client receives a STOMP message from the server
    if (message.body) {
        console.log("av 1: " + message.body)
    } else {
        console.log("got empty message");
    }
};

var message_callback_av2 = function (message) {
    // called when the client receives a STOMP message from the server
    if (message.body) {
        console.log("oem: " + message.body)
    } else {
        console.log("got empty message");
    }
};

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

client.connect('', '', connect_callback, error_callback);

sleep(1000).then(value => {
    var sub1 = client.subscribe('/topic/crash-event/av', message_callback_av1, {});
    var sub2 = client.subscribe('/topic/crash-event/oem/bmw', message_callback_av2, {});
    console.log(sub1);
    console.log(sub2);
    // client.send('/topic/greeting', {}, 'HELLO from stomp');
});


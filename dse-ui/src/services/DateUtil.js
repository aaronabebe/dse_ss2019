export const compareDates = (a, b) => {
    if (a.event.dateTime > b.event.dateTime) {
        return -1;
    }

    if (a.event.dateTime < b.event.dateTime) {
        return 1;
    }
    return 0;
};

export const formatDate = (dateString) => {
    let date = new Date(Date.parse(dateString));
    let minutes = date.getMinutes();
    let formattedMinutes = date.getMinutes() < 10 ? "0" + minutes : minutes;
    return `${date.getMonth()+1}/${date.getDate()}/${date.getFullYear()} ${date.getHours()}:${formattedMinutes}`
};
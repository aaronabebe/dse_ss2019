import Stomp from "stompjs";

export const initClient = (url, topic, handler) => {
    const client = Stomp.client(url);
    let subscription = null;
    client.connect('', '',
        () => {
            subscription = client.subscribe(topic, handler, {});
        },
        (e) => {
            if(e.includes("Lost connection")){
                console.log("Retrying connect in 5 seconds.")
                setTimeout(() => initClient(url, topic, handler), 1000);
            }
        });
    return subscription;
};

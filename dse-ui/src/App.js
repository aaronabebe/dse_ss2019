import React from "react";
import {HashRouter, NavLink, Route} from "react-router-dom";
import Home from "./components/Home";
import axiosInstance from "./services/axiosInstance";
import {initClient} from "./services/stompClientInstance";
import Header from "./components/navigation/Header";
import AvRoutes from "./components/navigation/routes/AvRoutes";
import OemRoutes from "./components/navigation/routes/OemRoutes";
import EmsRoute from "./components/navigation/routes/EmsRoute";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            vehicles: [],
            crashes: {},
            accidents: [],
            oems: [],
            currentPositions: {}
        };
    }

    componentDidMount() {
        this.getAllAccidents();
        this.getAllOems();
        this.getCurrentPositions();
        const crashSubscription = initClient(
            `ws://${process.env.REACT_APP_GATEWAY_URL}/notifications`,
            `/topic/crash-event/ems`,
            this.handleCrashEvent
        );
        this.setState({crashSubscription})
    }

    componentWillUnmount() {
        if (this.state.crashSubscription) {
            this.state.crashSubscription.unsubscribe();
        }
    }

    handleCrashEvent = (msg) => {
        const notification = JSON.parse(msg.body);
        const {crashes} = this.state;
        if (notification.notificationType === "deactivate-crash") {
            crashes[notification.content.id] = {};
        } else {
            crashes[notification.content.id] = notification.content;
        }
        this.setState({crashes});
    };

    getAllOems = () => {
        const uri = `http://${process.env.REACT_APP_GATEWAY_URL}/oems`;
        axiosInstance.get(uri).then(
            res => {
                const oems = res.data;
                this.setState({oems: oems});
                const {vehicles} = this.state;
                oems.forEach(oem => vehicles.push(...oem.avs));
                this.setState({vehicles});
            }, (e) => console.log(e)
        )
    };

    getAllAccidents = () => {
        const uri = `http://${process.env.REACT_APP_GATEWAY_URL}/accidents`;
        axiosInstance.get(uri)
            .then(res => {
                this.setState({
                    accidents: res.data,
                    crashes: res.data
                        .filter(accident => accident.status === "active")
                        .map(accident => accident.event)
                });
            }, (e) => console.log(e))
    };

    getCurrentPositions = () => {
        const uri = `http://${process.env.REACT_APP_GATEWAY_URL}/events/positions`;
        axiosInstance.get(uri)
            .then(res => {
                this.setState({currentPositions: res.data});
            }, (e) => console.log(e))
    };

    render() {
        return (
            <HashRouter>
                <div className="App">
                    <Header oems={this.state.oems} vehicles={this.state.vehicles} NavLink={NavLink}/>
                    <div className="content">
                        <Route exact path="/"
                               component={Home}/>
                        <EmsRoute getAllAccidents={this.getAllAccidents}
                                  accidents={this.state.accidents}/>
                        <OemRoutes oems={this.state.oems}
                                   crashes={Object.values(this.state.crashes)}
                                   currentPositions={Object.values(this.state.currentPositions)}/>
                        <AvRoutes vehicles={this.state.vehicles}
                                  oems={this.state.oems}
                                  crashes={Object.values(this.state.crashes)}
                                  currentPositions={Object.values(this.state.currentPositions)}/>
                    </div>
                </div>
            </HashRouter>
        );
    }
}

export default App;
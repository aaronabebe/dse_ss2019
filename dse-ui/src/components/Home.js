import React, {Component} from "react";
import axiosInstance from "../services/axiosInstance";

class Home extends Component {

    startDataGenerator = () => {
        const uri = `${process.env.REACT_APP_GATEWAY_URL}/generator/start`;
        axiosInstance.post(uri);
    };

    render() {
        return (
            <div className="dashboard text-center">
                <h1>Welcome to BackendUI.</h1>
                <h3>Click on an actor in the top right to begin monitoring.</h3>
            </div>
        );
    }
}

export default Home;
import React, {Component} from "react";
import EmsDashboard from "../../EmsDashboard";
import {Route} from "react-router-dom";

class EmsRoute extends Component {

    render () {
        return (
            <Route name="ems"
                   path="/ems"
                   render={(props) => <EmsDashboard {...props}
                                                    getAllAccidents={this.props.getAllAccidents}
                                                    accidents={this.props.accidents}
                   />}
            />
        );
    }
}

export default EmsRoute;
import React, {Component} from "react";
import Route from "react-router-dom/es/Route";
import AvDashboard from "../../AvDashboard";

class AvRoutes extends Component {

    findOem(oems, vehicle) {
        let oemName = "OEM NOT FOUND";
        oems.forEach(oem => {
            oem.avs.forEach(av => {
                console.log(av);
                if(av === vehicle){
                    oemName = oem.name;
                }
            });
        });
        return oemName;
    }

    render() {
        console.log(this.props);
        return (
            this.props.vehicles.map(vehicle => (
                <Route key={vehicle.chassisNumber}
                       name={vehicle.chassisNumber}
                       path={"/av/" + vehicle.chassisNumber}
                       render={(props) => <AvDashboard {...props}
                                                       vehicle={vehicle}
                                                       oem={this.findOem(this.props.oems, vehicle)}
                                                       crashes={this.props.crashes
                                                           .filter(crash => typeof crash.nearbyAvs !== "undefined" ?
                                                               crash.nearbyAvs.includes(vehicle.chassisNumber) : false)
                                                       }
                                                       getCurrentPosition={this.props.getCurrentPositions}
                                                       position={this.props.currentPositions
                                                           .filter(position => position.chassisNumber === vehicle.chassisNumber)
                                                       }
                       />}
                />
            ))
        );
    }
}

export default AvRoutes;
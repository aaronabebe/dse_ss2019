import React, {Component} from "react";
import Route from "react-router-dom/es/Route";
import OemDashboard from "../../OemDashboard";

class OemRoutes extends Component {

    render() {
        return (
            this.props.oems.map(oem => (
                    <Route key={oem.name}
                           name={oem.name}
                           path={"/oem/" + oem.name}
                           render={(props) => <OemDashboard {...props}
                                                            oem={oem}
                                                            getCurrentPositions={this.props.getCurrentPositions}
                                                            crashes={this.props.crashes
                                                                .filter(crash => crash.oem === oem.name)
                                                            }
                                                            currentPositions={this.props.currentPositions}
                           />}
                    />
                ))
        );
    }
}

export default OemRoutes;
import React, {Component} from "react";
import {Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink as ReactstrapNavLink} from "reactstrap";
import OemDropdown from "./dropdowns/OemDropdown";
import AvDropdown from "./dropdowns/AvDropdown";


class Header extends Component {

    constructor(props){
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        }
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        return (
            <header className="App-header">
                <Navbar color="light" light expand="md">
                    <NavbarBrand tag={this.props.NavLink} to="/">Autonomous Driving Monitor</NavbarBrand>
                    <NavbarToggler onClick={this.toggle}/>
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <OemDropdown tag={this.props.NavLink} oems={this.props.oems}/>
                            <NavItem>
                                <ReactstrapNavLink tag={this.props.NavLink} to="/ems"> EMS </ReactstrapNavLink>
                            </NavItem>
                            <AvDropdown tag={this.props.NavLink} vehicles={Object.values(this.props.vehicles)}/>
                        </Nav>
                    </Collapse>
                </Navbar>
            </header>
        );
    }
}

export default Header;
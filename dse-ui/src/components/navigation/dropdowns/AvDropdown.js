import React from "react";
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";


const AvDropdown = ({vehicles, tag}) => {
    return (
        <UncontrolledDropdown nav inNavbar>
            <DropdownToggle nav caret>
                Autonomous vehicles
            </DropdownToggle>
            <DropdownMenu right>
                {/* Generate these elements by currently active vehicles */}
                {vehicles.map(vehicle => (
                    <DropdownItem key={vehicle.chassisNumber}
                                  tag={tag}
                                  to={"/av/" + vehicle.chassisNumber}>{vehicle.modelType + ": "+ vehicle.chassisNumber}</DropdownItem>
                ))}
            </DropdownMenu>
        </UncontrolledDropdown>
    );
};
export default AvDropdown;
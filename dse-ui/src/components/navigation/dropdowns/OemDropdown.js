import React from "react";
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";


const OemDropdown = ({oems, tag}) => {
    return (
        <UncontrolledDropdown nav inNavbar>
            <DropdownToggle nav caret>
                OEMs
            </DropdownToggle>
            <DropdownMenu right>
                {/* Generate these elements by currently active vehicles */}
                {oems.map(oem => (
                    <DropdownItem key={oem.name}
                                  tag={tag}
                                  to={"/oem/" + oem.name}>{oem.name}</DropdownItem>
                ))}
            </DropdownMenu>
        </UncontrolledDropdown>
    );
};
export default OemDropdown;
import React, {Component} from "react";
import {Map, TileLayer} from 'react-leaflet'
import AccidentMarker from "./markers/AccidentMarker";


class EmsMap extends Component {

    constructor(props) {
        super(props);
        this.state = {
            lat: 48.195539120823256,
            lng: 16.45100770962156,
            zoom: 10
        }
    }

    componentDidMount() {
        const leafletMap = this.leafletMap.leafletElement;
        leafletMap.on('zoomend', () => {
            const updatedZoomLevel = leafletMap.getZoom();
            window.console.log('Current zoom level -> ', updatedZoomLevel);
            this.handleZoomLevelChange(updatedZoomLevel);
        });

        // leafletMap.on('')
    }

    handleZoomLevelChange(newZoomLevel) {
        this.setState({zoom: newZoomLevel});
    }

    render() {
        const position = [this.state.lat, this.state.lng];
        return (
            <Map style={{width: "100%", height: "100%"}} ref={m => {
                this.leafletMap = m;
            }} center={position} zoom={this.state.zoom}>
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
                />
                {this.props.accidents.map(accident => (
                    <AccidentMarker key={accident.id} accident={accident}/>
                ))}
            </Map>
        );
    }
}

export default EmsMap;
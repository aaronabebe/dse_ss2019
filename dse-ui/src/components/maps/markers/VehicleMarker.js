import {Marker, Popup} from "react-leaflet";
import React from "react";
import {renderToStaticMarkup} from 'react-dom/server';
import {divIcon} from "leaflet";

const iconMarkup = renderToStaticMarkup(<i className=" fas fa-car fa-3x" style={{color: "black"}}/>);
const carIcon = divIcon({
    html: iconMarkup,
});

const VehicleMarker = ({position}) => {
    return (
        <Marker icon={carIcon} position={[position.latitude, position.longitude]}>
            <Popup>
                {position.chassisNumber}
            </Popup>
        </Marker>
    );
};

export default VehicleMarker;
import {Marker, Popup} from "react-leaflet";
import React from "react";
import {renderToStaticMarkup} from "react-dom/server";
import {divIcon} from "leaflet";
import {formatDate} from "../../../services/DateUtil";


const iconMarkup = renderToStaticMarkup(<i className=" fa fa-car-crash fa-3x" style={{color: "red"}}/>);
const crashIcon = divIcon({
    html: iconMarkup,
});

const AccidentMarker = ({accident}) => {
    return (
        <Marker icon={crashIcon} position={[accident.event.latitude, accident.event.longitude]}>
            <Popup>
                <div className="text-center">
                    <p>{accident.event.crashEventDescription}</p>
                    {formatDate(accident.event.dateTime)}
                </div>
            </Popup>
        </Marker>
    );
};

export default AccidentMarker;
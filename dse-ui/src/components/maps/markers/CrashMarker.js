import {Marker, Popup} from "react-leaflet";
import React from "react";
import {renderToStaticMarkup} from "react-dom/server";
import {divIcon} from "leaflet";
import {formatDate} from "../../../services/DateUtil";

const iconMarkup = renderToStaticMarkup(<i className=" fa fa-car-crash fa-3x" style={{color: "red"}}/>);
const crashIcon = divIcon({
    html: iconMarkup,
});


const CrashMarker = ({crash}) => {
    return (
        <Marker icon={crashIcon} position={[crash.latitude, crash.longitude]}>
            <Popup>
                <div className="text-center">
                    <p>{crash.crashEventDescription}</p>
                    {formatDate(crash.dateTime)}
                </div>
            </Popup>
        </Marker>
    );
};

export default CrashMarker;
import React, {Component} from "react";
import {Map, TileLayer} from 'react-leaflet'
import VehicleMarker from "./markers/VehicleMarker";
import CrashMarker from "./markers/CrashMarker";


class OemMap extends Component {

    constructor(props) {
        super(props);
        this.state = {
            lat: 48.195539120823256,
            lng: 16.45100770962156,
            zoom: 10
        }
    }

    componentDidMount() {
        const leafletMap = this.leafletMap.leafletElement;
        leafletMap.on('zoomend', () => {
            const updatedZoomLevel = leafletMap.getZoom();
            window.console.log('Current zoom level -> ', updatedZoomLevel);
            this.handleZoomLevelChange(updatedZoomLevel);
        });
    }

    handleZoomLevelChange(newZoomLevel) {
        this.setState({zoom: newZoomLevel});
    }

    render() {
        const position = [this.state.lat, this.state.lng];
        console.log(this.props);
        return (
            <Map style={{width: "100%", height: "100%"}} ref={m => {
                this.leafletMap = m;
            }} center={position} zoom={this.state.zoom}>
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
                />
                {this.props.avPositions.map(avPosition => (
                    <VehicleMarker key={avPosition.chassisNumber} position={avPosition}/>
                ))}
                {this.props.crashes.map(crash => (
                    <CrashMarker key={crash.id} crash={crash}/>
                ))}
            </Map>
        );
    }
}

export default OemMap;
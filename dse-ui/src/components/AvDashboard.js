import React, {Component} from "react";
import Row from "reactstrap/es/Row";
import Col from "reactstrap/es/Col";
import {initClient} from "../services/stompClientInstance";
import AvMap from "./maps/AvMap";
import {alertStyle} from "../services/StyleObjects";
import Alert from "reactstrap/es/Alert";

class AvDashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            newAvPosition: null,
            crashes: this.props.crashes,
            crashEventVisible: false,
            initialPosition: this.props.position[0] ? this.props.position[0] : {}
        };
        console.log(this.state);
    }

    componentDidMount() {
        const positionSubscription = initClient(
            `ws://${process.env.REACT_APP_GATEWAY_URL}/ws/events`,
            `/topic/oems/${this.props.oem}/avs/positions`,
            this.handleAvPosition
        );

        const crashSubscription = initClient(
            `ws://${process.env.REACT_APP_GATEWAY_URL}/notifications`,
            `/topic/crash-event/av/${this.props.vehicle.chassisNumber}`,
            this.handleCrashEvent
        );
        this.setState({positionSubscription});
        this.setState({crashSubscription});
        console.log(this.props);
    }

    componentWillUnmount() {
        if (this.state.crashSubscription) {
            this.state.crashSubscription.unsubscribe();
        }
        if (this.state.positionSubscription) {
            this.state.positionSubscription.unsubscribe();
        }
    }

    handleCrashEvent = (msg) => {
        const notification = JSON.parse(msg.body);
        const {crashes} = this.state;
        if (notification.notificationType === "deactivate-crash") {
            crashes.pop(notification.content);
            this.setState({crashes, crashEventVisible: false});
        } else {
            crashes.push(notification.content);
            this.setState({crashes, crashEventVisible: true});
        }
    };

    handleAvPosition = (msg) => {
        const newAvPosition = JSON.parse(msg.body);
        if (newAvPosition.chassisNumber === this.props.vehicle.chassisNumber) {
            this.setState({newAvPosition});
        }
    };

    renderAlert = () => {
        if (this.state.crashEventVisible) {
            return <Alert style={alertStyle}
                          color='danger'
                          isOpen={this.state.crashEventVisible}
                          toggle={() => this.setState({crashEventVisible: false})}>
                New incoming Crash!
            </Alert>;
        }
    };

    render() {
        return (
            <div className="dashboard">
                <Row>
                    <Col>
                        <AvMap
                            position={this.state.newAvPosition ? this.state.newAvPosition : this.state.initialPosition}
                            crashes={this.state.crashes}/>
                    </Col>
                    <Col className="text-center" style={{height: "700px"}}>
                        <h3>
                            {this.props.oem + " " + this.props.vehicle.modelType}
                        </h3>
                        <h4>
                            Chassisnumber: {this.props.vehicle.chassisNumber}
                        </h4>
                    </Col>
                </Row>
                {this.renderAlert()}
            </div>
        )
            ;
    }
}

export default AvDashboard;
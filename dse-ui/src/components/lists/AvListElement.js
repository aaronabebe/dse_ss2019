import Card from "reactstrap/es/Card";
import React from 'react';
import CardHeader from "reactstrap/es/CardHeader";

const cardStyle = {
    width: '100%',
    marginBottom: "10px"
};


const AvListElement = ({vehicle, oem}) => {
    return (
        <Card style={cardStyle} className="text-center">
            <CardHeader>
                {oem + " " + vehicle.modelType + ": " + vehicle.chassisNumber}
            </CardHeader>
        </Card>
    );
};

export default AvListElement;

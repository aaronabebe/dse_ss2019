import ListGroup from "reactstrap/es/ListGroup";
import AvListElement from "./AvListElement";
import React from 'react';

const listStyle = {
    height: "700px",
    overflow: "scroll"
};

const AvList = ({vehicles, oem}) => {
    return (
        <ListGroup style={listStyle}>
            {vehicles.map(
                vehicle => (
                    <AvListElement key={vehicle.chassisNumber}
                                   vehicle={vehicle}
                                   oem={oem}/>
                )
            )}
        </ListGroup>
    );
};

export default AvList;
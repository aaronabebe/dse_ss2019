import Card from "reactstrap/es/Card";
import CardBody from "reactstrap/es/CardBody";
import React from 'react';
import CardHeader from "reactstrap/es/CardHeader";
import CardTitle from "reactstrap/es/CardTitle";
import CardText from "reactstrap/es/CardText";
import CardFooter from "reactstrap/es/CardFooter";
import {formatDate} from "../../services/DateUtil";

const cardStyle = {
    width: '100%',
    color: 'red',
    marginBottom: "10px"
};

const CrashListElement = ({crash}) => {
    console.log(crash);
    return (
        <Card style={cardStyle} className="text-center">
            <CardHeader>
                {crash.oem + " " + crash.modelType + ": " + crash.chassisNumber}
            </CardHeader>
            <CardBody>
                <CardTitle>
                    <h5>
                        {crash.crashEventDescription}
                    </h5>
                    <h5>
                    </h5>
                </CardTitle>
                <CardText>
                    Number of occupants: {crash.numberOfOccupants}
                </CardText>
            </CardBody>
            <CardFooter>
                {formatDate(crash.dateTime)}
            </CardFooter>
        </Card>
    );
};

export default CrashListElement;
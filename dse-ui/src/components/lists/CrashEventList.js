import ListGroup from "reactstrap/es/ListGroup";
import React from 'react';
import CrashListElement from "./CrashListElement";

const listStyle = {
    height: "700px",
    overflow: "scroll"
};

const CrashEventList = ({crashes}) => {
    return (
        <ListGroup style={listStyle}>
            {crashes.map(
                crash => (
                    <CrashListElement key={crash.id}
                                      crash={crash}/>
                )
            )}
        </ListGroup>
    );
};

export default CrashEventList;
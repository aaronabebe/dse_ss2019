import React from "react";
import ListGroup from "reactstrap/es/ListGroup";
import AccidentListElement from "./AccidentListElement";

const listStyle = {
    height: "700px",
    overflow: "scroll"
};

const AccidentList = ({accidents, showButton, setInactive}) => {
    return (
        <ListGroup style={listStyle}>
            {accidents.map(
                accident => (
                    <AccidentListElement key={accident.id}
                                         accident={accident}
                                         showButton={showButton}
                                         setInactive={setInactive}/>
                )
            )}
        </ListGroup>
    );
};
export default AccidentList;

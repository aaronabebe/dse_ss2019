import Card from "reactstrap/es/Card";
import CardBody from "reactstrap/es/CardBody";
import React from 'react';
import Button from "reactstrap/es/Button";
import CardHeader from "reactstrap/es/CardHeader";
import CardTitle from "reactstrap/es/CardTitle";
import CardText from "reactstrap/es/CardText";
import CardFooter from "reactstrap/es/CardFooter";
import {formatDate} from "../../services/DateUtil";

const cardStyle = {
    width: '100%',
    marginBottom: "10px"
};

const AccidentListElement = ({accident, showButton, setInactive}) => {
    return (
        <Card style={cardStyle} className="text-center">
            <CardHeader>
                {accident.event.oem + " " + accident.event.modelType + ": " + accident.event.chassisNumber}
            </CardHeader>
            <CardBody>
                <CardTitle>
                    <h5>
                        {accident.event.crashEventDescription}
                    </h5>
                </CardTitle>
                <CardText>
                    Number of occupants: {accident.event.numberOfOccupants}
                </CardText>
                {showButton ? <Button color="success" onClick={() => setInactive(accident.id)}>Inactive</Button> : null}
            </CardBody>
            <CardFooter>
                {formatDate(accident.event.dateTime)}
            </CardFooter>
        </Card>
    );
};

export default AccidentListElement;

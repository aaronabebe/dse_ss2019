import React, {Component} from "react";
import Row from "reactstrap/es/Row";
import Col from "reactstrap/es/Col";
import EmsMap from "./maps/EmsMap";
import AccidentList from "./lists/AccidentList";
import axiosInstance from "../services/axiosInstance";
import {initClient} from "../services/stompClientInstance";
import Alert from "reactstrap/es/Alert";
import {alertStyle} from "../services/StyleObjects";


class EmsDashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            crashes: this.props.crashes,
            accidents: this.props.accidents,
            crashEventVisible: false
        };
    }

    componentDidMount() {
        this.getAllAccidents();
        const subscription = initClient(
            `ws://${process.env.REACT_APP_GATEWAY_URL}/notifications`,
            "/topic/crash-event/ems",
            this.handleCrashEvent
        );
        this.setState({subscription});
    }

    getAllAccidents = () => {
        const uri = `http://${process.env.REACT_APP_GATEWAY_URL}/accidents`;
        axiosInstance.get(uri)
            .then(res => {
                this.setState({accidents: res.data});
            }, (e) => console.log(e))
    };

    componentWillUnmount() {
        if (this.state.subscription) {
            this.state.subscription.unsubscribe();
        }
    }

    handleCrashEvent = (msg) => {
        const notification = JSON.parse(msg.body);
        if (notification.notificationType === "activate-crash") {
            this.setState({crashEventVisible: true});
            this.getAllAccidents();
        } else {
            this.setState({crashEventVisible: false});
        }
        this.getAllAccidents();
    };

    setAccidentInactive = (id) => {
        const uri = `http://${process.env.REACT_APP_GATEWAY_URL}/accident/${id}`;
        axiosInstance.put(uri)
            .then(() => this.getAllAccidents())
            .catch((e) => console.log(e));
    };

    renderAlert = () => {
        if (this.state.crashEventVisible) {
            console.log("rendering state");
            return <Alert style={alertStyle}
                          color='danger'
                          isOpen={this.state.crashEventVisible}
                          toggle={() => this.setState({crashEventVisible: false})}>
                New incoming Crash!
            </Alert>;
        }
    };

    render() {
        return (
            <div className="dashboard">
                <Row>
                    <Col>
                        <EmsMap
                            accidents={this.state.accidents.filter(accident => accident.status === 'active')}/>
                    </Col>
                    <Col>
                        <h3>Active Accidents</h3>
                        <AccidentList
                            accidents={this.state.accidents.filter(accident => accident.status === 'active')}
                            showButton={true}
                            setInactive={this.setAccidentInactive}/>
                    </Col>
                    <Col>
                        <h3>All Accidents</h3>
                        <AccidentList accidents={this.state.accidents.sort(this.compareDates)} showButton={false}/>
                    </Col>
                </Row>
                {this.renderAlert()}
            </div>
        );
    }
}

export default EmsDashboard;
import React, {Component} from "react";
import OemMap from "./maps/OemMap";
import AvList from "./lists/AvList";
import CrashEventList from "./lists/CrashEventList";
import './OEM.css'
import Col from "reactstrap/es/Col";
import Row from "reactstrap/es/Row";
import {initClient} from "../services/stompClientInstance";
import {alertStyle} from "../services/StyleObjects";
import Alert from "reactstrap/es/Alert";

class OemDashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            avPositions: this.props.currentPositions,
            crashes: this.props.crashes,
            crashEventVisible: false
        };
    }

    componentDidMount() {
        const positionSubscription = initClient(
            `ws://${process.env.REACT_APP_GATEWAY_URL}/ws/events`,
            `/topic/oems/${this.props.oem.name}/avs/positions`,
            this.handleAvPosition
        );
        const crashSubscription = initClient(
            `ws://${process.env.REACT_APP_GATEWAY_URL}/notifications`,
            `/topic/crash-event/oem/${this.props.oem.name}`,
            this.handleCrashEvent
        );
        this.setState({positionSubscription});
        this.setState({crashSubscription});
    }

    handleCrashEvent = (msg) => {
        const notification = JSON.parse(msg.body);
        const {crashes} = this.state;
        if (notification.notificationType === "deactivate-crash") {
            crashes.pop(notification.content);
            this.setState({crashes, crashEventVisible: false});
        } else {
            crashes.push(notification.content);
            this.setState({crashes, crashEventVisible: true});
        }
    };

    handleAvPosition = (msg) => {
        const newAvPosition = JSON.parse(msg.body);
        const {avPositions} = this.state;
        avPositions[newAvPosition.chassisNumber] = newAvPosition;
        this.setState({avPositions});
    };

    renderAlert = () => {
        if (this.state.crashEventVisible) {
            console.log("rendering state");
            return <Alert style={alertStyle}
                          color='danger'
                          isOpen={this.state.crashEventVisible}
                          toggle={() => this.setState({crashEventVisible: false})}>
                New incoming Crash!
            </Alert>;
        }
    };

    componentWillUnmount() {
        if (this.state.positionSubscription) {
            this.state.positionSubscription.unsubscribe();
        }
        if (this.state.crashSubscription) {
            this.state.crashSubscription.unsubscribe();
        }
    }


    render() {
        return (
            <div className="dashboard">
                <Row>
                    <Col>
                        <OemMap
                            avPositions={Object.values(this.state.avPositions)
                                .filter(position => position.oem === this.props.oem.name)}
                            crashes={Object.values(this.state.crashes)}/>
                    </Col>
                    <Col>
                        <h3>Currently driving AVs</h3>
                        <AvList vehicles={this.props.oem.avs} oem={this.props.oem.name}/>
                    </Col>
                    <Col>
                        <h3>Crashes</h3>
                        <CrashEventList crashes={Object.values(this.state.crashes)}/>
                    </Col>
                </Row>
                {this.renderAlert()}
            </div>
        );
    }
}

export default OemDashboard;
package at.tuwien.dse.simulator;

import at.tuwien.dse.simulator.api.ClientWrapper;
import at.tuwien.dse.simulator.geom.Util;
import io.swagger.client.model.AVUpdateEvent;
import io.swagger.client.model.OEMDTO;
import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Main class to simulate vehicles moving along an axis (straight line)
 * Creates the new AVs at the server and sends avupdates periodically
 * for each AV.
 */
public class Simulator {
    private static final Logger LOGGER = LoggerFactory.getLogger(Simulator.class);

    private SimulatorProps properties = SimulatorProps.SCENARIO_VALIDATION();
    private List<AV> avs = new ArrayList<>();   // list of AVs ordered by their physical position
    private int it = 0; // current iteration

    private static ClientWrapper clientWrapper = new ClientWrapper();   // Backend API client

    public Simulator() {
        this.init();
    }

    public Simulator(SimulatorProps props) {
        this.properties = props;
        this.init();
    }

    /**
     * Initialize AVs to simulate and create them at the server.
     */
    private void init() {

        // init avs
        for (int i = 0; i < properties.numVehicles; i++) {
            AV av = new AV(String.valueOf(i),
                String.format("X%d", i),
                SimulatorProps.OEMS[i % SimulatorProps.OEMS.length],
                properties.startPositions[i][0],
                properties.startPositions[i][1],
                properties.speed,
                i < properties.secondsToCrash.length ? properties.secondsToCrash[i] : -1.0,
                new Random().nextInt(4) + 1);   // at least one occupant
            avs.add(av);
        }

        // post to server
        for (int i = 0; i < SimulatorProps.OEMS.length; i++) {
            String name = SimulatorProps.OEMS[i];
            OEMDTO oem = new OEMDTO()
                .name(name)
                .avs(avs.stream()
                    .filter(av -> av.oem.equals(name))
                    .map(AV::toAVDTO)
                    .collect(Collectors.toList()));
            clientWrapper.postOEM(oem);
        }
    }

    /**
     * Update cycle: update the each AV individually, then calculate
     * the distance between the previous and following vehicles and
     * send out the updates. If a vehicle has crashed, it will send
     * one last update and then stop sending updates.
     */
    public void update() {
        LOGGER.info("Updating... (it: {}, time: {})", it, it * properties.updateInterval);
        for (AV av : this.avs) {
            av.update(it, properties.updateInterval, properties.angle);
        }
        for (int i = 0; i < avs.size(); i++) {
            AV currAV = avs.get(i);

            if (!currAV.stopSendingUpdates) {
                calcDistToNextPrev(i, currAV);
                AVUpdateEvent avUpdate = createAVUpdateEvent(currAV);
                LOGGER.info("Sending update for av {}: [{}, {}], {}",
                    avUpdate.getChassisNumber(),
                    avUpdate.getLatitude(),
                    avUpdate.getLongitude(),
                    avUpdate.getCrashEventDescription());
                clientWrapper.postAvUpdate(avUpdate);

                if (currAV.hasCrashed()) {
                    currAV.stopSendingUpdates = true;
                }
            }
        }
    }

    /**
     * Create the AVUpdateEvent object with timestamp
     * from the AV
     *
     * @param av
     * @return AVUpdateEvent
     */
    private AVUpdateEvent createAVUpdateEvent(AV av) {
        return (AVUpdateEvent) new AVUpdateEvent()
            .distanceToNext(av.distToNext)
            .distanceToPrev(av.distToPrev)
            .sequenceNumber(it)
            .speed(av.speed)
            .chassisNumber(av.chassisNumber)
            .modelType(av.modelType)
            .oem(av.oem)
            .latitude(av.latitude)
            .longitude(av.longitude)
            .numberOfOccupants(av.numOccupants)
            .crashEventDescription(av.crashDescription)
            .dateTime(LocalDateTime.now().toString());
    }

    /**
     * Calculate the distance between currAV and previous and
     * following AVs in the list.
     *
     * @param idx
     * @param currAV
     */
    private void calcDistToNextPrev(int idx, AV currAV) {
        AV prevAV = idx - 1 >= 0 ? avs.get(idx - 1) : null;
        AV nextAV = idx + 1 < avs.size() ? avs.get(idx + 1) : null;

        if (prevAV != null) {
            currAV.distToPrev = Util.distance(currAV.latitude,
                prevAV.latitude,
                currAV.longitude,
                prevAV.longitude,
                200.0, 200.0) / 1000.;  // convert to km
        }
        if (nextAV != null) {
            currAV.distToNext = Util.distance(currAV.latitude,
                nextAV.latitude,
                currAV.longitude,
                nextAV.longitude,
                200.0, 200.0) / 1000.;  // convert to km
        }
    }

    /**
     * Start the simulation by using scheduled timer task.
     * Abort if the maxIt has been reached.
     */
    public void run() {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (properties.maxIt < 0 || it < properties.maxIt) {
                    update();
                    it++;
                } else {
                    LOGGER.info("Aborting simulation... at {} (maxit: {})", it, properties.maxIt);
                    this.cancel();
                    System.exit(1);
                }
            }
        };

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(task, 500, (long) this.properties.updateInterval * 1000);
    }


    /**
     * Property class to configure the simulator.
     */
    private static class SimulatorProps {
        private double updateInterval;  // seconds
        private double angle;
        private double speed;   // kmh
        private int numVehicles;
        private double[][] startPositions;  // should be in order
        private double[] secondsToCrash;   // seconds to crash, index corresponds to vehicle, -1 for no crash
        private int maxIt;

        private static final String[] OEMS = {"Tesla", "Fiat", "Renault", "Ford"};

        private SimulatorProps() {
        }

        public SimulatorProps(double updateInterval,
                              double angle,
                              double speed,
                              int numVehicles,
                              double[][] startPositions,
                              double[] secondsToCrash,
                              int maxIt
        ) {
            this.updateInterval = updateInterval;
            this.angle = angle;
            this.speed = speed;
            this.numVehicles = numVehicles;
            this.startPositions = startPositions;
            this.secondsToCrash = secondsToCrash;
            this.maxIt = maxIt;
        }

        /**
         * Property setting as required by the Verification scenario
         * from the specification
         *
         * @return
         */
        public static SimulatorProps SCENARIO_VALIDATION() {
            SimulatorProps p = new SimulatorProps();
            p.updateInterval = 1.0;  // seconds
            p.angle = 1.6401208046841111;
            p.speed = 200.0;
            p.numVehicles = 6;
            p.startPositions = new double[][]{
                {48.195539120823256, 16.45100770962156},
                {48.195729705620685, 16.44697040351769},
                {48.195983598790434, 16.441587282011607},
                {48.1964906311916, 16.430820879076272},
                {48.1973791, 16.415852},
                {48.199323956124516, 16.370255907818965}
            };
            p.secondsToCrash = new double[]{
                20.0, 60.0
            };
            p.maxIt = 1000;
            return p;
        }

    }


    public static void main(String[] args) {
        BasicConfigurator.configure();

        Simulator s = new Simulator();
        LOGGER.info("{}", s.avs);

        s.run();
    }


}
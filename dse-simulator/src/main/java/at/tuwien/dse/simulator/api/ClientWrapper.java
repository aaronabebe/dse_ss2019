package at.tuwien.dse.simulator.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.swagger.client.model.AVDTO;
import io.swagger.client.model.AVUpdateEvent;
import io.swagger.client.model.OEMDTO;
import org.glassfish.jersey.client.ClientConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 * Simple client jersey client only for the necessary methods to work
 * with the DSE application
 */
public class ClientWrapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientWrapper.class);

    private static final String URI = "http://dev.35.242.246.115.nip.io/api";   //"http://localhost:9000";

    Gson gson = new GsonBuilder().create();
    Client client = ClientBuilder.newClient(new ClientConfig());

    /**
     * Create an avupdate as json string to the webtarget
     *
     * @param avUpdate
     * @return response
     */
    public Response postAvUpdate(AVUpdateEvent avUpdate) {
        WebTarget webTarget = client.target(URI).path("av/update");
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(gson.toJson(avUpdate), MediaType.APPLICATION_JSON));
        LOGGER.info("{}", response.getStatus());
        return response;
    }

    /**
     * Create an OEM
     *
     * @param oem
     * @return response
     */
    public Response postOEM(OEMDTO oem) {
        WebTarget webTarget = client.target(URI).path("oem");
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.TEXT_PLAIN);
        Response response = invocationBuilder.post(Entity.entity(gson.toJson(oem), MediaType.APPLICATION_JSON));
        LOGGER.info("{}", response.getStatus());
        return response;
    }

    /**
     * Create an AV for an OEM
     *
     * @param av
     * @param oemName
     * @return response
     */
    public Response postAV(AVDTO av, String oemName) {
        WebTarget webTarget = client.target(URI).path(String.format("oem/%s/av", oemName));
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.TEXT_PLAIN);
        Response response = invocationBuilder.post(Entity.entity(gson.toJson(av), MediaType.APPLICATION_JSON));
        LOGGER.info("{}", response.getStatus());
        return response;
    }

}

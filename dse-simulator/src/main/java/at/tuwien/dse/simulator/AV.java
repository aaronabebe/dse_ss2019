package at.tuwien.dse.simulator;

import at.tuwien.dse.simulator.geom.Util;
import io.swagger.client.model.AVDTO;

public class AV {

    // static params
    public String chassisNumber;
    public String modelType;
    public String oem;
    public double startLatitude;
    public double startLongitude;
    public double speed;
    public double secondsToCrash;
    public int numOccupants;

    // dynamic params
    public double latitude;
    public double longitude;
    public double distToPrev = -1.0;
    public double distToNext = -1.0;
    public String crashDescription = null;
    public boolean stopSendingUpdates = false;

    public AV(String chassisNumber,
              String modelType,
              String oem,
              double startLatitude,
              double startLongitude,
              double speed,
              double secondsToCrash,
              int numOccupants) {
        this.chassisNumber = chassisNumber;
        this.modelType = modelType;
        this.oem = oem;
        this.startLatitude = this.latitude = startLatitude;
        this.startLongitude = this.longitude = startLongitude;
        this.speed = speed;
        this.secondsToCrash = secondsToCrash;
        this.numOccupants = numOccupants;
    }

    /**
     * Update the position by calculating the distance travelled in the time interval.
     * Sets the crashDescription if the current time has exceeded the secondsToCrash.
     *
     * @param step
     * @param updateInterval
     * @param movingAngle
     */
    public void update(int step, double updateInterval, double movingAngle) {
        if (!hasCrashed()) {
            double[] latLon = Util.calcNewLatLon(latitude, longitude, speed * updateInterval / 3600., movingAngle);
            latitude = latLon[0];
            longitude = latLon[1];

            if (secondsToCrash > 0. && (step * updateInterval > secondsToCrash)) {
                crashDescription = String.format("AV %s %s %s crashed due to animal crossing. There so much meat!", chassisNumber, modelType, oem);
            }
        }
    }

    public boolean hasCrashed() {
        return crashDescription != null;
    }

    public AVDTO toAVDTO() {
        return new AVDTO().chassisNumber(chassisNumber).modelType(modelType);
    }

    public String toString() {
        return String.format("AV[%s, %s, %s, %f, %f, %f, %f, %f]",
            chassisNumber, modelType, oem, startLatitude, startLongitude, speed, latitude, longitude);
    }
}

package at.tuwien.dse.simulator.geom;

/**
 * Geographic utility functions
 */
public final class Util {

    private static final double earthRadius = 6371.0;

    /**
     * TAKEN FROM: https://stackoverflow.com/questions/19352921/how-to-use-direction-angle-and-speed-to-calculate-next-times-latitude-and-longi
     * Calculate new lat long coordinates by going a distance at an angle from a starting point
     *
     * @param lat
     * @param lon
     * @param dist
     * @param angle
     * @return array with lat long
     */
    public static double[] calcNewLatLon(double lat, double lon, double dist, double angle) {
        double newLat = Math.asin(Math.sin(Math.toRadians(lat)) * Math.cos(dist / earthRadius) +
            Math.cos(Math.toRadians(lat)) * Math.sin(dist / earthRadius) * Math.cos(angle));
        double newLon = Math.toRadians(lon) +
            Math.atan2(Math.sin(angle) * Math.sin(dist / earthRadius) * Math.cos(Math.toRadians(lat)),
                Math.cos(dist / earthRadius) - Math.sin(Math.toRadians(lat)) * Math.sin(newLat));

        return new double[]{Math.toDegrees(newLat), Math.toDegrees(newLon)};
    }

    /**
     * TAKEN FROM: https://stackoverflow.com/questions/3694380/calculating-distance-between-two-points-using-latitude-longitude
     * Calculate distance between two points in latitude and longitude taking
     * into account height difference. If you are not interested in height
     * difference pass 0.0. Uses Haversine method as its base.
     * <p>
     * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters
     * el2 End altitude in meters
     *
     * @returns Distance in Meters
     */
    public static double distance(double lat1, double lat2, double lon1,
                                  double lon2, double el1, double el2) {

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
            + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
            * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = earthRadius * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }
}

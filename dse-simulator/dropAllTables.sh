#!/usr/bin/env bash

sudo docker exec -it dsess2019_mongodb_1 mongo entitystore --eval "db.dropDatabase();"
sudo docker exec -it dsess2019_mongodb_1 mongo eventstore --eval "db.dropDatabase();"
sudo docker exec -it dsess2019_mongodb_1 mongo notificationsstore --eval "db.dropDatabase();"
